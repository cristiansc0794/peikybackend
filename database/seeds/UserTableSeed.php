<?php

use Illuminate\Database\Seeder;

class UserTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users=[
            [
                'name'=>'prueba martinez',
                'email'=>'prueba@gmail.com',
                'phone'=>'123456',
                'password'=>Hash::make('123456'),
                'type_users'=>'admin',
                'state'=>'1',
                'cancelled'=>'0',
                'typeofservice'=>'1',

            ]
        ];

        foreach ($users as $user){
            \App\User::create($user);
        }
    }
}
