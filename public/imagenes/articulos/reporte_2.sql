-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-03-2017 a las 01:18:16
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gift2all`
--

-- --------------------------------------------------------

--
-- Estructura para la vista `reporte_2`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `reporte_2`  AS  select distinct `p`.`nit` AS `nit`,`p`.`nombre` AS `marca`,`p`.`convenio_redeban` AS `convenio_redeban`,`l`.`nombre` AS `local`,`l`.`id` AS `id_local`,`l`.`id_beacon` AS `id_beacon`,`l`.`ciudad` AS `ciudad`,`r`.`code` AS `codigo_bowgift`,`r`.`code` AS `codigo_redeban`,`r`.`valor` AS `valor_original`,`r`.`fecha_comprado` AS `fecha_venta`,`m`.`valor_movimiento` AS `valor_redimido`,`m`.`fecha_transaccion` AS `fecha_redencion`,`m`.`hora_movimiento` AS `hora_redencion`,`c`.`nombre_completo` AS `nombre_regala`,`c`.`celular` AS `telefono_regala`,`c`.`correo` AS `mail_regala`,`r`.`nombre_to` AS `nombre_recibe`,`r`.`numero_to` AS `telefono_recibe`,`r`.`correo_to` AS `mail_recibe` from ((((`mvs` `m` left join `regalos` `r` on((`m`.`codigo_tarjeta` = `r`.`code`))) left join `proveedors` `p` on((`r`.`id_marca` = `p`.`id`))) left join `clientes` `c` on((`r`.`id_cliente` = `c`.`id`))) join `locals` `l` on((`m`.`codigo_terminal` = `l`.`code_terminal`))) ;

--
-- VIEW  `reporte_2`
-- Datos: Ninguna
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
