var url='http://localhost:8000/';
var id_store;
var storemodal;
function cargardatos(id) {
    var nombret;
    id_store=id;
    $.get(url+'admin/catego/'+id,function(resultado){
        $('#tablaA').DataTable().destroy();
        console.log(resultado.data);
        eliminarbtn();
        eliminaFilas();
        if (resultado.data.length!=0){
                $.each(resultado.data, function(key,val) {
                    var row = $("<tr><td>" + val.name+"</td><td><img src='"+url+"imagenes/articulos/"+val.logo+"' height='100px' width='100px' class='img-thumbnail'></td><td><a class='btn btn-info' data-toggle='tooltip' onclick='edtCate("+val.id+")' title='Editar categoria' ><i class='fa fa-pencil'></i> </a><button class='btn btn-danger elim' data-toggle='tooltip' title='Eliminar categoria' tienda='"+id+"' id='"+val.id+"'> <i class='fa fa-trash-o' aria-hidden='true'></i></button><a class='btn btn-default-light verproductcat' data-toggle='tooltip' title='Ver productos de la categoria' tienda='"+id+"' id='"+val.id+"'> <i class='fa fa-lemon-o' aria-hidden='true'></i></a></td></tr>");
                    $("#tablaA").append(row);
                });
        }
        var btn=$("<button class='btn btn-success crearcategoteinda' id='"+id+"'>Crear Categoria</button><button type='button' class='btn btn-danger' data-dismiss='modal'>Cerrar</button>");
        $("#footermodalid").append(btn);
        nombret=$("#namestore"+id).html();
        $("#tittlelistcat").html('Tienda: '+nombret);
        $('#tablaA').DataTable(
            {
                "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "NingÃºn dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Ãšltimo",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            }
        ).draw();
        $('#myModal').modal('show');
    });
}

function edtCate(id){

    $('#myModal').modal('hide');
    $("#modalEditCat").modal();

    $.get(url+'admin/categoid/'+id,function(resultado){
        

        var res=resultado.data;

        $('#idcat').val(res.id);
        $('#namecat').val(res.name);

        $('#logocat').attr({ value: '' });
        //$('#logocategory').val(res.logo);
        $('#idstore').val(res.id_store);
    });
}

$('#updatecatego').on('click',function (event) {
    var formData = new FormData(document.getElementById("formUpdateCatego"));

    $.ajax({
        url: url+'admin/catego/update',
        type: "POST",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    }).done(function(resultado){
        cargardatos(id_store);
        $("#divnamecatedit").attr('class','form-group');
        $("#emnamecatedit").html("");
        $('#modalEditCat').modal('hide');
        $("#myModal").modal();

    }).fail(function( jqXHR, textStatus, errorThrown ) {
        if(jqXHR.status == 500){
            if(jqXHR.responseJSON.errors.namecat){
                $("#divnamecatedit").attr('class','form-group has-error');
                $("#emnamecatedit").html(jqXHR.responseJSON.errors.namecat[0]);
            }else{
                $("#divnamecatedit").attr('class','form-group');
                $("#emnamecatedit").html("");
            }
        }
    });
});

$("#btncancel_edt").click(function () {
    $('#modalEditCat').modal('hide');
    $('#myModal').modal('show');
    $("#divnamecatedit").attr('class','form-group');
    $("#emnamecatedit").html("");
});

$('#myModal').on('click', '.verproductcat', function () {
    var categoria = $(this).attr('id');
    var tienda = $(this).attr('tienda');
    storemodal=tienda;
    window.location.href=url+"admin/product/"+tienda+"/"+categoria;
});


$('#myModal').on('click', '.elim', function () {
    var id = $(this).attr('id');
    var tienda = $(this).attr('tienda');
    $.get(url+'admin/catego/delete/'+id,function(resultado){
        if (resultado.respuesta=="ok"){
            console.log("eliminado");
            cargardatos(tienda)
        }
    });
});

$('#myModal').on('click', '.crearcategoteinda', function () {
    resetearform();
    eliminarhidden();
    var id = $(this).attr('id');
    $('#myModal').modal('hide');
    var input=$("<input type='hidden' id='tienda' name='id_store' value='"+id+"'/>");
    $("#idtiendahidden").append(input);
    $('#myModalcreatecat').modal('show');
    console.log(id);
});

function resetearform() {
    document.getElementById("Registrocatego").reset();
}

function eliminaFilas()
{
    var n=0;
    $("#tablaA tbody tr").each(function ()
    {
        n++;
    });
    for(i=n;i>=0;i--)
    {
        $("#tablaA tbody tr:eq('"+i+"')").remove();
    };
};

function eliminarhidden() {
    var n=0;
    $("#idtiendahidden input").each(function ()
    {
        n++;
    });
    for(i=n;i>=0;i--)
    {
        $("#idtiendahidden input:eq('"+i+"')").remove();
    };
}

function  eliminarbtn() {
    var n=0;
    $("#footermodalid button").each(function ()
    {
        n++;
    });
    for(i=n;i>=0;i--)
    {
        $("#footermodalid button:eq('"+i+"')").remove();
    };
}

$("#crearcatego").click(function () {
    $('#myModal').modal('hide');
    $('#myModalcreatecat').modal('show');
});

$("#cancelcatego").click(function () {
    $('#myModalcreatecat').modal('hide');
    $('#myModal').modal('show');
    $("#divnamecat").attr('class','form-group');
    $("#divnamecaterror").html("");
    $("#divlogocat").attr('class','form-group');
    $("#divlogocaterror").html("");
});

$("#enviarcatego").on('click',function (event) {
    event.preventDefault();
    var formData = new FormData(document.getElementById("Registrocatego"));

    $.ajax({
        url: url+'admin/categoria/crear',
        type: "POST",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    }).done(function(resultado){
        console.log(resultado);
        var tienda=$("#tienda").val();
        $("#divnamecat").attr('class','form-group');
        $("#divnamecaterror").html("");
        $("#divlogocat").attr('class','form-group');
        $("#divlogocaterror").html("");
        $('#myModalcreatecat').modal('hide');
        cargardatos(tienda);
    }).fail(function( jqXHR, textStatus, errorThrown ,resultado) {
        if(jqXHR.status == 500){
            if(jqXHR.responseJSON.errors.name){
                $("#divnamecat").attr('class','form-group has-error');
                $("#divnamecaterror").html(jqXHR.responseJSON.errors.name[0]);
            }else{
                $("#divnamecat").attr('class','form-group');
                $("#divnamecaterror").html("");
            }
            if(jqXHR.responseJSON.errors.logo){
                $("#divlogocat").attr('class','form-group has-error');
                $("#divlogocaterror").html("El Logo de la cateogria es requerido con un peso menor de 4mb.");
            }else{
                $("#divlogocat").attr('class','form-group');
                $("#divlogocaterror").html("");
            }
            console.log(jqXHR)
        }
    });
});