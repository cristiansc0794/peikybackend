<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table='products';
    protected $primaryKey="id";
    protected $fillable = [
        'name', 'description', 'precio','imagen','cantidad','id_category'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category' ,'id_category');
    }

    public function promotions()
    {
        return $this->hasMany('App\Promotion');
    }

    public function sales()
    {
        return $this->belongsToMany('App\Sale');
    }
}
