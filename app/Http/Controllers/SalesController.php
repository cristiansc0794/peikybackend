<?php

namespace App\Http\Controllers;

use App\Product;
use App\Store;
use Illuminate\Http\Request;
use League\Flysystem\Exception;
use App\Sale;
use App\Detalle_Sale;
use Carbon\Carbon; //para usar fecha y hora de la zona horaria
use Response;
use DB;
use Tymon\JWTAuth\Facades\JWTAuth;
class SalesController extends Controller
{
    public function setstore(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $store=Store::where('id_users', '=', $user->id)->first();
        try{
            DB::beginTransaction();
            $sale= new Sale();
            $sale->id_store=$store->id;
            $mytime=Carbon::now('America/Bogota');
            $sale->date=$mytime->toDateTimeString();
            $sale->total_sale=0;
            $sale->save();

            $producs=json_decode($request->get('idproducs'));
            $prices=json_decode($request->get('prices'));
            $quantities=json_decode($request->get('quantities'));
            $total_sale=0;
            $contador=0;
            while ($contador<count($producs)){
                $detallesale= new Detalle_Sale();
                $exist=Product::join('categories', 'categories.id', '=', 'products.id_category')
                    ->join('stores', 'stores.id', '=', 'categories.id_store')
                    ->select('products.id')
                    ->where('stores.id', '=', $store->id)
                    ->where('products.id', '=', $producs[$contador])
                    ->first();
                if($exist){
                    $produc=Product::find($producs[$contador]);
                    if($produc->cantidad>=$quantities[$contador]){
                        $detallesale->price=$prices[$contador]*$quantities[$contador];
                        $total_sale=$total_sale+($prices[$contador]*$quantities[$contador]);
                        $detallesale->quantity=$quantities[$contador];
                        $detallesale->id_product=$producs[$contador];
                        $detallesale->id_sale=$sale->id;
                        $produc->cantidad=($produc->cantidad-$quantities[$contador]);
                        $produc->update();
                        $detallesale->save();
                    }
                }
                $contador+=1;
            }
            $saleModific=Sale::find($sale->id);
            $saleModific->total_sale=$total_sale;
            $saleModific->update();

            DB::commit();
        }catch (Exception $ex){
            DB::rollback();
        }
        return response()->json('Venta realizada',200);
    }

    public function topproductstore(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $store=Store::where('id_users', '=', $user->id)->first();
        $productsend=[];

        $products=Store::join('sales','sales.id_store','=','stores.id')
                ->join('detalle_sales','detalle_sales.id_sale','=','sales.id')
                ->join('products','detalle_sales.id_product','=','products.id')
                ->select('detalle_sales.id_product as idproduct','products.name as nombre',DB::raw('SUM(detalle_sales.quantity) as total_sales'))
                ->groupBy(['idproduct','nombre'])
                ->orderBy('total_sales', 'desc')
                ->limit(5)
                ->where('stores.id','=',$store->id)->get();

                foreach ($products as $pro){
                    $objeto=new \stdClass();
                    $objeto->idproduct=$pro->idproduct;
                    $objeto->nombre=$pro->nombre;
                    $objeto->total_sales=(integer)$pro->total_sales;
                    array_push($productsend,$objeto);
                }

        return response()->json($productsend,200);
    }

    public function fiveMonthsProduct(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $store=Store::where('id_users', '=', $user->id)->first();

        $date=Carbon::now('America/Bogota');
        $dateInit=$date->toDateTimeString();
        $dateEndconvert=$date->subMonths(5);
        $dateEnd=$dateEndconvert->toDateTimeString();
        $productsend=[];


        $products=Store::join('sales','sales.id_store','=','stores.id')
            ->join('detalle_sales','detalle_sales.id_sale','=','sales.id')
            ->select(DB::raw('SUM(detalle_sales.quantity) as total_sales'),DB::raw('YEAR(sales.date) year, MONTH(sales.date) month'))
            ->groupBy('year','month')
            //->orderBy('sales.date', 'desc')
            ->where('stores.id','=',$store->id)
            ->where('detalle_sales.id_product','=',$request->get('idproduct'))
            ->whereBetween('sales.date', [$dateEnd, $dateInit])
            ->get();

            foreach ($products as $pro){
                $objeto=new \stdClass();
                $objeto->total_sales=(integer)$pro->total_sales;
                //$array = explode("-", $pro->date);
                //$objeto->date= (integer)$array[1];
                $objeto->year=$pro->year;
                $objeto->month=$pro->month;
                array_push($productsend,$objeto);
            }
            /*
             for ($i=0; $i <= count($productsend)-1; $i++){
                for ($x=$i+1; $x <= count($productsend)-1; $x++){
                    if($productsend[$i]->date==$productsend[$x]->date){
                        $productsend[$i]->total_sales=$productsend[$i]->total_sales+$productsend[$x]->total_sales;
                    }
                }
            }
            for ($i=0; $i <= count($productsend)-1; $i++){
                for ($x=$i+1; $x <= count($productsend)-1; $x++){
                    unset($productsend[$x]);
                }
            }
             $sendproduct =array_values($productsend);*/
            return response()->json($productsend,200);
    }

    public function gainSellers()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $store=Store::where('id_users', '=', $user->id)->first();

        $date=Carbon::now('America/Bogota');
        $dateInit=$date->toDateTimeString();
        $dateEndconvert=$date->subMonths(5);
        $dateEnd=$dateEndconvert->toDateTimeString();


        $products=Store::join('sales','sales.id_store','=','stores.id')
            ->join('detalle_sales','detalle_sales.id_sale','=','sales.id')
            ->select(DB::raw('SUM(detalle_sales.price) as total_sold'),DB::raw('YEAR(sales.date) year, MONTH(sales.date) month'))
            ->groupBy('year','month')
            ->where('stores.id','=',$store->id)
            ->whereBetween('sales.date', [$dateEnd, $dateInit])
            ->get();


        return response()->json($products,200);
    }
}
