<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
class StoreController extends Controller
{
    protected function validator(array $data)
    {
        $messages = [
            'required' => 'The :attribute es requerido.',
        ];

        return Validator::make($data, [
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',

        ],$messages);
    }

    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'message' => $errors
            ], 500);
        }
            $user = JWTAuth::parseToken()->authenticate();
            $store=Store::where('id_users', '=', $user->id)->first();
            if($store){
                return response()->json(["Error"=>"Usuario ya tiene creado una tienda"],500);
            }
            $store=new Store();
            $store->id_users=$user->id;
            $store->name=$request->get('name');
            $store->phone=$request->get('phone');
            $store->address=$request->get('address');
            if($store->save()){
                return response()->json(["success"=>"Store guardado"],200);
            }
            return response()->json(["Error"=>"error al guardar store"],500);
    }

    public function show()
    {
            $token = JWTAuth::getToken();
            if($token){
                    $user = JWTAuth::parseToken()->authenticate();
                    $store=Store::where('id_users', '=', $user->id)->first();
                    if($store){
                        $store = Store::find($store->id);
                        return response()->json(compact('store'));
                    }
                return response()->json(["Error"=>"usuario no tiene tienda registrada"],500);
           }
           return response()->json(["Error"=>"error token no generado"],500);
    }

    public function destroy(Request $request)
    {
            $token = JWTAuth::getToken();
            if($token){
                    $user = JWTAuth::parseToken()->authenticate();
                    $store=Store::where('id_users', '=', $user->id)->first();
                    if($store){
                        $store = Store::find($store->id);
                        if($store->delete()){
                            return response()->json(["succes"=>"Store eliminado"],200);
                        }else{
                            return response()->json(["error"=>"Error al Eliminar"],500);
                        }
                    }
                return response()->json(["Error"=>"usuario no tiene tienda registrada"],500);
            }
           return response()->json(["Error"=>"error token no generado"],500);
    }

    public function update(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errors = json_decode($errors);

            return response()->json([
                'success' => false,
                'message' => $errors
            ], 500);
        }
            $token = JWTAuth::getToken();
            if($token){
                $user = JWTAuth::parseToken()->authenticate();
                $store=Store::where('id_users', '=', $user->id)->first();
                if($store){
                    $store = Store::find($store->id);
                    $store->id_users=$user->id;
                    $store->name=$request->get('name');
                    $store->phone=$request->get('phone');
                    $store->address=$request->get('address');
                    if($store->update()){
                        return response()->json(["success"=>"Store modificado"],200);
                    }
                    return response()->json(["Error"=>"error al modificado store"],500);
                }
                return response()->json(["Error"=>"usuario no tiene tienda registrada"],500);
            }
            return response()->json(["Error"=>"error token no generado"],500);
    }

}
