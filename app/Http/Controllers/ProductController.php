<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Category;
use App\Store;
use App\User;
use Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProductController extends Controller
{
    protected function validator(array $data)
    {
        $messages = [
            'required' => 'The :attribute es requerido.',
        ];

        return Validator::make($data, [
            'name' => 'required',
            'description' => 'required',
            'precio' => 'required',
            'imagen' => 'required',
            'cantidad' => 'required',
            'id_category' => 'required',
        ],$messages);
    }

    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'message' => $errors
            ], 500);
        }
        $user = JWTAuth::parseToken()->authenticate();
        if($user->typeofservice=='2' and $user->cancelled=='0'){
            $products=Product::join("categories","categories.id","=","products.id_category")
                ->join("stores","stores.id","=","categories.id_store")
                ->select('products.id','products.name','products.description','products.precio','products.imagen','products.cantidad','products.id_category')
                ->where('stores.id_users','=',$user->id)
                ->get();
            if(count($products)>=4){
                return response()->json('Usuario no ha pagado ah llegado 4 productos limite',500);
            }
        }
        $store=Store::where('id_users', '=', $user->id)->first();
        $category=Category::find($request->get('id_category'));
        if(Category::where('id', '=', $request->get('id_category'))->exists()){
            if($store->id==$category->id_store){
                $producto=new Product();
                $producto->name=$request->get('name');
                $producto->description=$request->get('description');
                $producto->precio=$request->get('precio');
                $producto->imagen=$request->get('imagen');
                $producto->cantidad=$request->get('cantidad');
                $producto->id_category=$request->get('id_category');
                if($producto->save()){
                    $respuesta=["success"=>"Producto guardado","producto"=>$producto];
                    return response()->json(compact('respuesta'),200);
                }
                return response()->json(["Error"=>"Error al crear producto"],500);
            }
            return response()->json(["Error"=>"categoria no existe en tu tienda"],500);
        }
        return response()->json(["Error"=>"categoria no existe"],500);
    }

    public function allProduct()
    {
        $token = JWTAuth::getToken();
        if($token){
            $user = JWTAuth::parseToken()->authenticate();
            $products=Product::join("categories","categories.id","=","products.id_category")
                ->join("stores","stores.id","=","categories.id_store")
                ->select('products.id','products.name','products.description','products.precio','products.imagen','products.cantidad','products.id_category')
                ->where('stores.id_users','=',$user->id)
                ->get();
            return response()->json(["success"=>$products],200);
        }
        return response()->json(["error"=>"token_not"],500);
    }

    public function destroy(Request $request)
    {
        if(!empty($request->get('id')) && Product::where('id', '=', $request->get('id'))->exists()){
            $user = JWTAuth::parseToken()->authenticate();
            $products=Product::join("categories","categories.id","=","products.id_category")
                ->join("stores","stores.id","=","categories.id_store")
                ->select('products.name','products.description','products.precio','products.imagen','products.cantidad','products.id_category')
                ->where('stores.id_users','=',$user->id)
                ->where('products.id','=',$request->get('id'))
                ->first();
            if($products==null){
                return response()->json(["Error"=>"Producto no encontrado"],500);
            }else{
                $product=Product::find($request->get('id'));
                if($product->delete()){
                    return response()->json(["success"=>"Producto eliminado"],200);
                }
                return response()->json(["Error"=>"Error al eliminar producto"],500);
            }
        }
        return response()->json(["Error"=>"Error producto no encotrado"],500);
    }

    public function update(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'message' => $errors
            ], 500);
        }
        if(!empty($request->get('id')) && Product::where('id', '=', $request->get('id'))->exists()){
            $user = JWTAuth::parseToken()->authenticate();
            $products=Product::join("categories","categories.id","=","products.id_category")
                ->join("stores","stores.id","=","categories.id_store")
                ->select('products.name','products.description','products.precio','products.imagen','products.cantidad','products.id_category')
                ->where('stores.id_users','=',$user->id)
                ->where('products.id','=',$request->get('id'))
                ->first();
            if($products==null) {
                return response()->json(["Error" => "Producto no encontrado"], 500);
            }
            $store=Store::where('id_users', '=', $user->id)->first();
            $category=Category::find($request->get('id_category'));
            if(Category::where('id', '=', $request->get('id_category'))->exists()){
                if($store->id==$category->id_store){
                    $producto=Product::find($request->get('id'));
                    $producto->name=$request->get('name');
                    $producto->description=$request->get('description');
                    $producto->precio=$request->get('precio');
                    $producto->imagen=$request->get('imagen');
                    $producto->cantidad=$request->get('cantidad');
                    $producto->id_category=$request->get('id_category');
                    if($producto->update()){
                        return response() ->json(["success"=>$producto]);
                    }
                    return response()->json(["Error"=>" Al actualizar producto"],500);
                }
                return response()->json(["Error"=>" Categoria no encotrada"],500);
            }
            return response()->json(["Error"=>"Error categoria no encotrada"],500);
        }
        return response()->json(["Error"=>"Error producto no encotrado"],500);
    }

}
