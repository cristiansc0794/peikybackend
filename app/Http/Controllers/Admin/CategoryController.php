<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Routing\Controller as Controller;
use App\User;
use App\Store;
use App\Category;
use DB;
use App\Product;
use Illuminate\Support\Facades\Input;
use Validator;

class CategoryController extends Controller
{
    protected function validator(array $data)
    {
        $messages = [
            'name.required' => 'El Nombre de la categoria es requerido.',
            'logo.required' => 'El Logo de la cateogria es requerido con un peso menor de 4mb.',
        ];

        return Validator::make($data, [
            'name' => 'required',
            'logo' => 'required',

        ],$messages);
    }

    public function category($id)
    {
        $category = Category::where('id', $id)->first();
        return response()->json(['data'=>$category]);
    }

    public function create()
    {
        $tiendas=Store::all();
        return view('admin.category.create',['tiendas'=>$tiendas]);

    }

    public function update(Request $request)
    {
        $messages=[
            'namecat.required' => 'El Nombre de la categoria es requerido.',
        ];
        if($request->ajax()){
            $validator = Validator::make($request->all(), [
                'namecat' => 'required'
            ],$messages);

            if ($validator->fails()){
                $errors = $validator->errors();
                $errors =  json_decode($errors);

                return response()->json([
                    'success' => false,
                    'errors' => $errors
                ], 500);
            }
           /* if ($validator->fails()) {
                return redirect('post/create')
                    ->withErrors($validator)
                    ->withInput();
            }*/
            $cat = Category::find($request->idcat);
            $cat->name = $request->namecat;

            if (!empty($request->logocat)) {

                if(Input::hasFile('logocat')){
                    $file=Input::file('logocat');
                    $cat->logo=$file->getClientOriginalName();
                    $file->move(public_path().'/imagenes/articulos/', $cat->logo);
                }
            }

            $cat->save();

            return response()->json(["succes"=>"Categoria guardada"],200);
        }
    }

    public function store(Request $request)
    {
        if($request->ajax()){
            $validator = $this->validator($request->all());
            if ($validator->fails()) {
                $errors = $validator->errors();
                $errors =  json_decode($errors);

                return response()->json([
                    'success' => false,
                    'errors' => $errors
                ], 500);
            }
            if (filesize(Input::file("logo"))!=0){
                $cateogria=new Category();
                $cateogria->name=$request->get('name');
                if(Input::hasFile('logo')){
                    //$rand=random_int(0, 999);
                    $file=Input::file('logo');
                    $cateogria->logo=$file->getClientOriginalName();
                    $file->move(public_path().'/imagenes/articulos/', $cateogria->logo);
                }
                $cateogria->id_store=$request->get('id_store');
                $cateogria->save();
                return response()->json(["succes"=>"Categoria guardada"],200);
            }
                return response()->json(["error"=>"Imagen pasa de tamaño"],500);

        }

    }

    public function delete($id)
    {
        $cate=Category::where('id','=',$id);
        if($cate){
            $cate=Category::find($id);
            $cate->delete();
            return response()->json(["respuesta"=>"ok"],200);
        }
        return response()->json(["respuesta"=>"error"],500);
    }

    public function categoriaportiendas($id)
    {
        $categorias= Category::join("stores","stores.id","=","categories.id_store")
            ->select('categories.id as id','categories.name as name','categories.logo as logo','stores.name as tienda')
            ->where("stores.id","=",$id)
            ->get();
        return response()->json(['data'=>$categorias]);
    }

}