<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Routing\Controller as Controller;
use App\User;
use App\Store;
use App\Category;
use App\Product;
use DB;
use Illuminate\Support\Facades\Input;
use Validator;

class ProductController extends Controller
{
    public function index()
    {
    	/*$productos= Product::join("categories","categories.id","=","products.id_category")
    		->select('products.id as id','products.name as name', 'products.description as description', 'products.precio as price', 'products.imagen as image', 'products.cantidad as quantity', 'categories.name as category')
            ->get();*/

        $productos = DB::table('products')
            ->join('categories', 'categories.id', '=', 'products.id_category')
            ->join('stores', 'stores.id', '=', 'categories.id_store')
            ->join('users', 'users.id', '=', 'stores.id_users')
            ->select('products.id as id','products.name as name', 'products.description as description', 'products.precio as price', 'products.imagen as image', 'products.cantidad as quantity', 'categories.name as category', 'stores.name as store', 'users.name as user')
            ->orderBy('categories.name')
            ->get();

        return view('admin.stores.product.product', [
            'products' =>$productos,
            //'categories' =>$categorias
            ]);
    }

    public function create($tienda,$categoria)
    {
        $tiend=Store::find($tienda);
        $catego=Category::find($categoria);
        if($tiend){
            if($catego){
                if ($tiend->id==$catego->id_store){
                    return view('admin.stores.product.create',['tienda'=>$tienda,'categoria'=>$categoria]);
                }
                return redirect('/error');
            }
            return redirect('/error');
        }
        return redirect('/error');
        
    }

    public function store(Request $request)
    {
        $tiend=$request->get('id_tienda');
        $cat=$request->get('id_category');

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'precio' => 'required',
            'imagen' => 'required',
            'cantidad' => 'required',
            'id_category' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('admin/product/create/'.$tiend.'/'.$cat)
                ->withErrors($validator)
                ->withInput();
        }

        $producto=new Product();
        $producto->name=$request->get('name');
        $producto->description=$request->get('description');
        $producto->precio=$request->get('precio');
        $producto->cantidad=$request->get('cantidad');
        $producto->id_category=$cat;
        if(!Input::file("imagen"))
        {
            return redirect('admin/product/create/'.$tiend.'/'.$cat);
        }

        if(Input::hasFile('imagen')){
            //$rand=random_int(0, 999);
            $file=Input::file('imagen');
            $producto->imagen=$file->getClientOriginalName();
            $file->move(public_path().'/imagenes/productos/',$producto->imagen);
        }
        $producto->save();
        return redirect('admin/product/'.$tiend.'/'.$cat);
    }


    public function edit($tienda,$cateogoria,$producto)
    {

        $producto = DB::table('products')
            ->join('categories', 'categories.id', '=', 'products.id_category')
            ->join('stores', 'stores.id', '=', 'categories.id_store')
            ->select('products.id', 'products.name','products.description','products.precio','products.cantidad','products.id_category','stores.id as idtiendap')
            ->where('stores.id', '=', $tienda)
            ->where('categories.id', '=', $cateogoria)
            ->where('products.id', '=', $producto)
            ->first();
        if(empty($producto)){
            return redirect('/error');
        }

        $categorias=Category::join('stores', 'stores.id', '=', 'categories.id_store')
            ->select('categories.id','categories.name')
            ->where('stores.id', '=', $tienda)->get();



        return view('admin.stores.product.edit', [
            'product' =>$producto,
            'categories' =>$categorias,
            ]);
    }

    public function update(Request $req)
    {
        $tienda=$req->get('idtiendap');
        $catego=$req->get('id_category');
        $product=Product::find($req->get('id'));
        $product->name=$req->get('name');
        $product->description=$req->get('description');
        $product->precio=$req->get('precio');
        if(Input::file("imagen")){
            $file=Input::file('imagen');
            $product->imagen=$file->getClientOriginalName();
            $file->move(public_path().'/imagenes/productos/',$product->imagen);
        }

        $product->cantidad=$req->get('cantidad');
        $product->id_category=$req->get('id_category');
        $product->update();
        return redirect('admin/product/'.$tienda.'/'.$catego);
    }

    public function delete($tienda,$categoria,$producto)
    {
        $product = DB::table('products')
            ->join('categories', 'categories.id', '=', 'products.id_category')
            ->join('stores', 'stores.id', '=', 'categories.id_store')
            ->select('products.id', 'products.name','products.description','products.precio','products.cantidad','products.id_category','stores.id as idtiendap')
            ->where('stores.id', '=', $tienda)
            ->where('categories.id', '=', $categoria)
            ->where('products.id', '=', $producto)
            ->first();
        if(!empty($product)){
            $pro=Product::find($producto);
            $pro->delete();
            return redirect('admin/product/'.$tienda.'/'.$categoria);
        }
        return redirect('/error');
    }

    public function productporcategoria($tienda,$categoria)
    {
        $products=Product::join("categories","categories.id","=","products.id_category")
            ->join("stores","stores.id","=","categories.id_store")
            ->select('products.id','products.name','products.description','products.precio','products.imagen','products.cantidad','products.id_category','categories.name as nameC','stores.name as nameS')
            ->where('stores.id','=',$tienda)
            ->where('categories.id','=',$categoria)
            ->get();
        $tien=Store::find($tienda);
        $cat=Category::find($categoria);
        if($tien){
            if ($cat){
                if ($tien->id==$cat->id_store){
                    return view('admin.stores.product.product', [
                        'products' =>$products,
                        'nombretienda' =>$tien->name,
                        'nombrecatego' =>$cat->name,
                        'tienda'=>$tienda,
                        'categoria'=>$categoria
                    ]);
                }
                    return redirect('/error');
            }
            return redirect('/error');
        }
        return redirect('/error');
    }

}