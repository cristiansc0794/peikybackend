<?php

namespace App\Http\Controllers\Admin;
use App\Store;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as Controller;
use Mail;
use Validator;

class UserController extends Controller {

	public function index() {
		$userstore = Store::select('id_users')->get();
		//dd($userstore);
		$users = User::whereNotIn('users.id', $userstore)->get();
		$usuarios = User::join("stores", "stores.id_users", "=", "users.id")
			->select('users.id as id', 'users.name as name', 'users.email as email', 'users.phone as phone', 'users.type_users as type_users', 'users.state as state', 'stores.name as tienda', 'stores.id as storeid')
			->get();

		// dd($users);
		return view('admin.users.users', [
			'users' => $usuarios,
			'usuarioN' => $users,
		]);
	}

	public function edit($id) {
		$usuario = User::find($id);
        if($usuario) {
            return view('admin.users.edit', [
                'user' => $usuario,
            ]);
        }
        return redirect('/error');
	}

	public function editartienda($id) {
		$store = Store::join('users', 'users.id', '=', 'stores.id_users')
			->select('users.name as nameU', 'stores.name as nameS', 'stores.phone as phone', 'stores.address as address', 'stores.id as id')
			->where('users.id', '=', $id)->first();
		return view('admin.users.tienda', ['tienda' => $store]);
	}

	public function updatetienda(Request $request) {
		$store = Store::find($request->get('id'));
		$store->name = $request->get('name');
		$store->phone = $request->get('phone');
		$store->address = $request->get('address');
		$store->update();
		return redirect('admin/store');

	}

	public function delete($id) {
		$user = User::where('id', '=', $id)->first();
		if ($user->state == 1) {
			$user->state = "0";
			$user->update();
			return redirect('admin/user');
		}
		$user->state = "1";
		$user->update();
		return redirect('admin/user');
	}

	public function create() {
		return view('admin.users.create');
	}

	public function store(Request $req) {

		$user = $req['email'];
		$name = $req['name'];
		$pass = $req['password'];

		$resultado = User::create([
			'name' => $req['name'],
			'email' => $req['email'],
			'password' => bcrypt($req['password']),
			'phone' => $req['phone'],
            'type_users'=>'provider',
            'state'=>'1',
            'cancelled'=>'0',
            'typeofservice'=>'1',
		]);

		if ($resultado = true) {

			/*Mail::send('emailing.email_cli', ['$user' => $user], function ($client) use ($user) {
				$client->from('grimorum@grimorum.com', 'Registro Exitoso');
				$client->to($user)->subject('Registro Exitoso');

			});*/

			return redirect('admin/user');
		}
	}

	public function update(Request $req) {
		// dd($req->all());

		User::where('id', $req['id'])->update(['name' => $req['name'],
			'email' => $req['email'],
			'password' => bcrypt($req['password']),
			'phone' => $req['phone'],
			'type_users' => $req['type_users'],
			'typeofservice' => $req['typeofservice'],
			'state' => $req['state'],
		]);

		return redirect('admin/user');
	}

	public function changePass(Request $req) {

        $messages=[
            'password.required' => 'El campo contraseña debe ser llenado.',
            'password_confirmation.required' => 'El campo de confirmacion debe ser llenado.',
            'password_confirmation.same' => 'Contraseñas no son iguales.',
        ];
        $validator = Validator::make($req->all(), [
            'password' => 'required',
            'password_confirmation' => 'required|same:password'
        ],$messages);

        if ($validator->fails()) {
            return redirect('admin/edit/'.$req['id'])
                ->withErrors($validator)
                ->withInput();
        }

		User::where('id', $req['id'])->update([
			'password' => bcrypt($req['password']),
		]);

		return redirect('admin/edit/' . $req['id']);
	}

}