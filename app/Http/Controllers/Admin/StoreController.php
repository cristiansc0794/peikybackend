<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Routing\Controller as Controller;
use Illuminate\Http\Request;
use App\User;
use App\Store;
use App\Category;
use App\Product;
use DB;
use Session;

class StoreController extends Controller
{
    public function index($idt=null)
    {
        if($idt==null){
            $tiendas= Store::join("users", "users.id","=", "stores.id_users")
                ->select('stores.id as id','stores.name as name', 'stores.phone as phone', 'stores.address as address', 'users.name as username', 'users.id as userid','stores.state as state')
                ->get();

            return view('admin.stores.store', [
                'stores' =>$tiendas,
                //'categories' =>$categorias
            ]);
        }else{
            $tien=Store::find($idt);
            if($tien){
                $tiendas= Store::join("users", "users.id","=", "stores.id_users")
                    ->select('stores.id as id','stores.name as name', 'stores.phone as phone', 'stores.address as address', 'users.name as username', 'users.id as userid','stores.state as state')
                    ->get();

                return view('admin.stores.store', [
                    'stores' =>$tiendas,
                    'idtc'=>$idt
                ]);
            }
            return redirect('/error');

        }

    }


    public function category($id)
    {
        $category = Category::where('id', $id)->first();
        $products = DB::table('products')->where('id_category', $id)->get();
        return view('admin.stores.category', [
    		'products' => $products,
            'category' => $category
    		]);
    }

    public function create()
    {
        $userstore=Store::select('id_users')->get();
        $users = User::whereNotIn('users.id', $userstore)->get();
        return view('admin.stores.create',['users'=>$users]);
    }

    public function store(Request $request)
    {
        $store=new Store();
        $store->name=$request->get('name');
        $store->phone=$request->get('phone');
        $store->state="1";
        $store->address=$request->get('address');
        if ($request->get('id_users')==null){
            Session::flash('error','Debe seleccionar un usuario');
            return redirect('admin/store/create');
        }
        $store->id_users=$request->get('id_users');
        $store->save();
        return redirect('admin/store');
    }

    public function productCreate($idCategoria)
    {
        $category = Category::where('id', $idCategoria)->first();
        return view('admin.stores.product.create', [
    		'category' => $category,
    		]);
    }

    public function productCreated(Request $request)
    {
        Product::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'precio' => $request['precio'],
            'imagen' => $request['imagen'],
            'cantidad' => $request['cantidad'],
            'id_category' => $request['id_category'],

        ]);

        return redirect('admin/store/category/'.$request['id_category']);
    }

    
    public function productDelete($id)
    {
        Product::destroy($id);
        return redirect('admin/user');
    }
    
    public function productEdit($id)
    {
        $product = Product::find($id);
        return view('admin.stores.product.edit', [
    		'product' =>$product
    		]);
    }
    public function productEdited(Request $req)
    {
        Product::where('id', $req['id'])->update(['name' => $req['name'],
                                        'description' => $req['description'],
                                        'precio' => $req['precio'],
                                        'imagen' => $req['imagen'],
                                        'cantidad' => $req['cantidad']
                                        ]);
        return redirect('admin/user');
    }

    public function categoryCreate($idTienda)
    {
        return view('admin.stores.category.create', [
            'idTienda' => $idTienda,
            ]);
    }
    public function categoryCreated(Request $req)
    {
        Category::create([
            'name' => $req['name'],
            'logo' => $req['logo'],
            'id_store' => $req['id_store']
        ]);

        return redirect('admin/store/'.$req['id_store']);
    }

    public function delete($id)
    {
        $store=Store::where('id','=',$id)->first();
        if ($store){
            $stor=Store::find($id);
            if($stor->state==1){
                $stor->state="0";
                $stor->update();
                return redirect('admin/store');
            }
            $stor->state=1;
            $stor->update();
        }
        return redirect('admin/store');
    }

    public function propietario($id)
    {

        $store= Store::join("users", "users.id","=", "stores.id_users")
            ->select('stores.id as id','stores.name as name', 'stores.phone as phone', 'stores.address as address', 'users.name as username')
            ->where('stores.id','=',$id)->first();




        $userstore=Store::select('id_users')->get();
        $users = User::whereNotIn('users.id', $userstore)->get();
        $store= Store::join("users", "users.id","=", "stores.id_users")
            ->select('stores.id as id','stores.name as name', 'stores.phone as phone', 'stores.address as address', 'users.name as username')
            ->where('stores.id','=',$id)->first();

        if ($store){
            return view('admin.stores.edit',['users'=>$users,'store'=>$store]);
        }else
            return redirect('/error');
    }

    public function updatepropietario(Request $request)
    {
            $id=$request->get('id');
            $tienda=Store::find($request->get('id'));
            $tienda->name=$request->get('name');
            $tienda->phone=$request->get('phone');
            $tienda->address=$request->get('address');
            if(!empty($request->get('id_users'))){
                $tienda->id_users=$request->get('id_users');
            }
            $tienda->update();
            return redirect('admin/store');

       // return redirect()->route('admin.store.edipropie',['id'=>$id]);
    }
}