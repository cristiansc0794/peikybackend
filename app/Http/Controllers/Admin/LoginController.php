<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Routing\Controller as Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Session;
use Redirect;


class LoginController extends Controller
{
    public function index()
    {
      if (Auth::check()) {
        return redirect('admin/indexadmin');
      }else{
        return view("admin/login");
      }
    }

    public function log(Request $request)
    {
    	// return $request->get('email');
    	$email = $request->get('email');
    	$password = $request->get('password');
    	# code...
           	
    	if (Auth::attempt(['email' => $email, 'password'=> $password])) {

            $tipo = Auth::user()->type_users;
            $estado = Auth::user()->state;
            if ($tipo!="admin" && $estado=="1"){
                Auth::logout();
                Session::flash('error','Usted no es un administrador');
                return redirect()->route('admin.login');
                //return Redirect::to('admin/index');
            }elseif ($tipo=="admin" && $estado!="1"){
                Auth::logout();
                Session::flash('error','Su cuenta esta dada de baja');
                return redirect()->route('admin.login');
            }
            return Redirect::to('admin/indexadmin');
        }
        Session::flash('error','Error en las credenciales');
        return redirect()->route('admin.login');
    }


    public function template(){

      return view("admin/index");
    }

     public function logout()
    {
      Auth::logout();
      return redirect::to('admin/');
    }

}