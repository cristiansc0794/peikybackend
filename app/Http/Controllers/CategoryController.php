<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\User;
use App\Store;
use Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class CategoryController extends Controller
{
    protected function validator(array $data)
    {
        $messages = [
            'required' => 'The :attribute es requerido.',
        ];

        return Validator::make($data, [
            'name' => 'required',
            'logo' => 'required',
        ],$messages);
    }

    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'message' => $errors
            ], 500);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $store=Store::where('id_users', '=', $user->id)->first();
        $category=new Category();
        $category->name=$request->get('name');
        $category->logo=$request->get('logo');
        $category->id_store=$store->id;
        if ($category->save()){
            return response()->json(["succes"=>$category],200);
        }
        return response()->json(["error"=>"fallo en la creacion categoria"],500);
    }

    public function shearname(Request $request)
    {
        $categoria=Category::join("stores","stores.id","=","categories.id_store")
            ->select('categories.id','categories.name')
            ->where('stores.id_users','=',1)
            ->where('categories.name','=',$request->get('name'))
            ->first();
        if ($categoria){
            return response()->json(["success"=>$categoria],200);
        }
        return response()->json(["Error"=>"Categoria no existe"],500);
    }

    public function allCategory()
    {
        $token = JWTAuth::getToken();
        if($token){
            $user = JWTAuth::parseToken()->authenticate();
            $categories=Category::join("stores","stores.id","=","categories.id_store")
                ->select('categories.id','categories.name','categories.logo','categories.id_store','stores.id_users')
                ->where('stores.id_users','=',$user->id)
                ->get();
            if($categories){
                return response()->json(["success"=>$categories],200);
            }
            return response()->json(["Error"=>"Tienda no tiene asiganadas categorias"],500);
        }
        return response()->json(["error"=>"token_not"],500);
    }

    public function show()
    {
        $token = JWTAuth::getToken();
        if($token){
            return response()->json(["success"=>"Entrastes"]);
        }
    }

    public function destroy(Request $request)
    {
        if(!empty($request->get('id')) && Category::where('id', '=', $request->get('id'))->exists()){
            $user = JWTAuth::parseToken()->authenticate();
            $store=Store::where('id_users', '=', $user->id)->first();
            $category=Category::find($request->get('id'));
            if($store->id==$category->id_store){
                if($category->delete()){
                    return response()->json(["succes"=>"Categoria eliminada"],200);
                }else{
                    return response()->json(["error"=>"Error al Eliminar"],500);
                }
            }
            return response()->json(["error"=>"categoria no pertenece a su tienda"],500);
        }
        return response()->json(["error"=>"Error objeto con id ".$request->get('id')." no existe"],500);
        
    }

    public function update(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'message' => $errors
            ], 500);
        }
        if(!empty($request->get('id')) && Category::where('id', '=', $request->get('id'))->exists()){
            $user = JWTAuth::parseToken()->authenticate();
            $store=Store::where('id_users', '=', $user->id)->first();
            $category=Category::find($request->get('id'));
            if($store->id==$category->id_store){
                $category->name=$request->get('name');
                $category->logo=$request->get('logo');
                $category->id_store=$store->id;
                if ($category->update()){
                    return response()->json(["succes"=>"Categoria actulziada","category"=>$category],200);
                }
                return response()->json(["error"=>"fallo en la actualizacion de la categoria"],500);
            }
            return response()->json(["error"=>"categoria no pertenece a su tienda"],500);
        }
        return response()->json(["error"=>"Error objeto con id ".$request->get('id')." no existe"],500);
    }
}
