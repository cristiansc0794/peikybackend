<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Inscripcion;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\Welcome;
use App\Mail\resetpassword;
use Illuminate\Support\Facades\Crypt;

class InscripcionesController extends Controller
{


    protected function validator(array $data)
    {
        $messages = [
            'required' => 'The :attribute es requerido.',
        ];

        return Validator::make($data, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',


        ],$messages);
    }

    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
       /* if ($validator->fails()) {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'message' => $errors
            ], 500);
        }*/
         if ($validator->fails()) {
                return redirect('/')
                    ->withErrors($validator)
                    ->withInput();
            }
        $inscripcion= new Inscripcion();
        $nombre=$request->get('name');
        $correo=$request->get('email');
        $telefono=$request->get('phone');

        $inscripcion->name=$nombre;
        $inscripcion->email=$correo;
        $inscripcion->phone=$telefono;
        if($inscripcion->save()){
            Mail::to('santirach07@gmail.com')->send(new Welcome($nombre,$correo,$telefono));
            return redirect('/');
        }

    }

    public function emailresetpass(Request $request)
    {
        $user=User::where('email','=',$request->get('email'))->first();
        if($user){
            Mail::to($request->get('email'))->send(new resetpassword($user->name,$user->email,Crypt::encrypt($user->id)));
            return redirect('/');
        }
        return redirect('/recuperarpass');
    }

    public function formresetpass($id)
    {
        $iduse = Crypt::decrypt($id);
        $user=User::where('id','=',$iduse)->first();
        if($user){
            return view('auth.passwords.reset',['id'=>Crypt::encrypt($user->id)]);
        }
    }

    public function updateresetpass(Request $request)
    {
        $iduser=Crypt::decrypt($request->get('iduser'));
        $messages=[
            'password.required' => 'El campo contraseña debe ser llenado.',
            'password_confirmation.required' => 'El campo de confirmacion debe ser llenado.',
            'password_confirmation.same' => 'Contraseñas no son iguales.',
        ];
        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'password_confirmation' => 'required|same:password'
        ],$messages);

        if ($validator->fails()) {
            return redirect()->route('form.reset.pass',["id"=>Crypt::encrypt($iduser)])
                ->withErrors($validator)
                ->withInput();
        }else{
            $user=User::where('id','=',$iduser)->first();
            $use=User::find($user->id);
            $use->password=bcrypt($request->get('password'));
            if($use->update()){
                return redirect()->route('admin.login');
            }
        }
    }

}

