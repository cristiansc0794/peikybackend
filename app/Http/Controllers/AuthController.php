<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;
use Validator;
use Carbon\Carbon; //para usar fecha y hora de la zona horaria

class AuthController extends Controller
{
    protected function validator(array $data)
    {
        $messages = [
            'email.required' => 'El campo email esta vacio por favor llenelo!',
            'email.unique'=>'Email ya existe',
            'password.required' => 'El campo password esta vacio por favor llenelo!',
            'required' => 'The :attribute es requerido.',
            'same'    => 'The :attribute and :other must match.',
            'size'    => 'The :attribute must be exactly :size.',
            'between' => 'The :attribute must be between :min - :max.',
            'in'      => 'The :attribute must be one of the following types: :values',
        ];

        return Validator::make($data, [
            'email' => 'required|unique:users',
            'password' => 'required|min:6',
            'name'=>'required',
            'phone'=>'required',
            //'type_users'=>'required',
            //'state'=>'required',

        ],$messages);
    }


    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'message' => $errors
            ], 500);
        }
        $exp = strtotime("+1 week");
        $customClaims = ['exp' => $exp];
        $user= new User();
        $user->name=$request->get('name');
        $user->email=$request->get('email');
        $user->password=Hash::make($request->get('password'));
        $user->phone=$request->get('phone');
        $user->type_users="provider";
        $user->state="1";
        $user->cancelled="0";
        $user->typeofservice="1";
        if($user->save()){
            $token = JWTAuth::fromUser($user, $customClaims);
            return Response()->json(compact('token'), 200);
        }
        return Response()->json(["error"=>"Error al registrar usuario"], 500);
    }

    public function login(Request $request)
    {

        $credenciales=$request->only('email','password');
        $token=null;
        try{
            $exp = strtotime("+1 week");
            $customClaims = ['exp' => $exp];
            $token=JWTAuth::attempt($credenciales,$customClaims);
            if(!$token){
                return response()->json(['error'=>'credenciales_invalidas'],500);
            }
        }catch (JWTException $exception){
            return response()->json(['error'=>'error'],500);
        }
        $user = JWTAuth::toUser($token);
        return response()->json(compact('token','user'));
    }
    public function logout()
    {
        $token = JWTAuth::getToken();
        if(!$token){
            return response()->json(['response'=>'Token not provided']);
        }
        try{
            JWTAuth::invalidate($token);
        } catch(TokenInvalidException $e){
            return response()->json(['response'=>'The token is invalid'], $e->getStatusCode());
        } catch(TokenBlacklistedException $e){
            return response()->json(['response'=>'The token is blacklisted'], $e->getStatusCode());
        }
        return response()->json(['response'=>'logged out successfully'], 200);
    }

    public function get_user_details()
    {
        //$value = Request::header('Authorization');
        $user = JWTAuth::parseToken()->authenticate();
        return response()->json(['result' => $user]);
    }

    public function registerface(Request $request)
    {
        $exp = strtotime("+1 week");
        $customClaims = ['exp' => $exp];
        $user= new User();
        $user->name=$request->get('name');
        $user->email=$request->get('email');
        $user->password=Hash::make(rand(100000, 1000000));
        $user->phone=$request->get('phone');
        $user->type_users="provider";
        $user->state="1";
        $user->cancelled="0";
        $user->typeofservice="1";
        if($user->save()){
            $token = JWTAuth::fromUser($user, $customClaims);
            return Response()->json(compact('token'), 200);
        }
        return Response()->json(["error"=>"Error al registrar usuario"], 500);
    }

    public function loginface(Request $request)
    {
        $user=User::where('email', '=', $request->get('email'))->first();
        if($user){
            $exp = strtotime("+1 week");
            $customClaims = ['exp' => $exp];
            $token = JWTAuth::fromUser($user,$customClaims);
            return response()->json(compact('token','user'));
        }else{
            $exp = strtotime("+1 week");
            $customClaims = ['exp' => $exp];
            $user= new User();
            $user->name=$request->get('name');
            $user->email=$request->get('email');
            $user->password=Hash::make(rand(100000, 1000000));
            $user->phone=$request->get('phone');
            $user->type_users="provider";
            $user->state="1";
            $user->cancelled="0";
            $user->typeofservice="1";
            if($user->save()){
                $token = JWTAuth::fromUser($user, $customClaims);
                $msj="Usuario registrado";
                return Response()->json(compact('token','msj'),200);
            }
            return Response()->json(["error"=>"Error al registrar usuario"], 500);
        }

    }

    public function checkPaid()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $date=Carbon::now('America/Bogota');
        $datepay=$date->diffInDays($user->created_at);
        if($user->typeofservice=='1'){
            if($datepay>4){
                if($user->cancelled=='0'){
                    return Response()->json(["error"=>"Usuario no ha pagado"], 500);
                }
            }
        }
        //$date1 = Carbon::createFromDate(1970,2,12);
    }

    public function paycanceled()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $user->cancelled='1';
        $user->update();
        return Response()->json(["success"=>'Pago realizado',"user"=>$user], 200);
    }
}
