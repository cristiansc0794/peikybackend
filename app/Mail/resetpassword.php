<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class resetpassword extends Mailable
{
    public $nombre;
    public $correo;
    public $id;
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nombre,$correo,$id)
    {
        $this->id=$id;
        $this->nombre=$nombre;
        $this->correo=$correo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from("peyki@grimorum.com","peyki");
        $this->subject('Cambio de contraseña');
        return $this->view('emails.passwordreset');
    }
}
