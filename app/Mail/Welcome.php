<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Welcome extends Mailable
{
    use Queueable, SerializesModels;
    public $nombre;
    public $correo;
    public $telefono;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nombre,$correo,$telefono)
    {
        $this->nombre=$nombre;
        $this->correo=$correo;
        $this->telefono=$telefono;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from($this->correo,$this->nombre);
        $this->subject('Nuevo contacto inscrito');
        return $this->view('emails.usermail');


    }
}
