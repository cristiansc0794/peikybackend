<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $table='promotions';
    protected $primaryKey="id";
    protected $fillable = [
        'name', 'description', 'id_product'
    ];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
