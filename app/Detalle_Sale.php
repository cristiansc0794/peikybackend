<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalle_Sale extends Model
{
    protected $table='detalle_sales';
    protected $primaryKey="id";
    protected $fillable = [
        'price', 'quantity', 'id_product','id_sale'
    ];
}
