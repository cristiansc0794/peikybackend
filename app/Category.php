<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table='categories';
    protected $primaryKey="id";
    protected $fillable = [
        'name', 'logo', 'id_store'
    ];

    public function store()
    {
        return $this->belongsTo('App\Store');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }

}
