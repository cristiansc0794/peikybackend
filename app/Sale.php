<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $table='sales';
    protected $primaryKey="id";
    protected $fillable = [
        'id_store','date','total_sale'
    ];

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function store()
    {
        return $this->belongsTo('App\Store');
    }
}
