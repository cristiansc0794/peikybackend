<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $table='stores';
    protected $primaryKey="id";
    protected $fillable = [
        'name', 'phone', 'address','state','id_users'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function categories()
    {
        return $this->hasMany('App\Category');
    }

    public  function sales()
    {
        return $this->hasMany('App\Sale');
    }
}
