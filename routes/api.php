<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');

//Route::post('registerface', 'AuthController@registerface');
Route::post('loginface', 'AuthController@loginface');

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('checkPaid', 'AuthController@checkPaid');
    Route::get('paycanceled', 'AuthController@paycanceled');


    Route::get('logout', 'AuthController@logout');
    Route::get('user_datails', 'AuthController@get_user_details');

    Route::get('stores/show', 'StoreController@show');
    Route::post('stores/store', 'StoreController@store');
    Route::post('stores/destroy', 'StoreController@destroy');
    Route::post('stores/update', 'StoreController@update');

    Route::get('categories/all', 'CategoryController@allCategory');
    //Route::get('categories/show', 'CategoryController@show');
    Route::post('categories/store', 'CategoryController@store');
    Route::post('categories/destroy', 'CategoryController@destroy');
    Route::post('categories/update', 'CategoryController@update');

    Route::get('products/all', 'ProductController@allProduct');
    //Route::get('products/show', 'ProductController@show');
    Route::post('products/store', 'ProductController@store');
    Route::post('products/destroy', 'ProductController@destroy');
    Route::post('products/update', 'ProductController@update');

    Route::post('sales/store', 'SalesController@setstore');

    Route::post('stadistic/producttop', 'SalesController@topproductstore');
    Route::post('stadistic/fivemonths', 'SalesController@fiveMonthsProduct');
    Route::post('stadistic/gainsellers', 'SalesController@gainSellers');
});

