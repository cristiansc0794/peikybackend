<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/recuperarpass', function () {
    return view('auth.passwords.email');
});

Route::get('/formresetpass/{id}',[
    'uses'=>'InscripcionesController@formresetpass',
    'as'=>'form.reset.pass'
]);
Route::post('inscripciones', 'InscripcionesController@store');
Route::post('emailresetpass', 'InscripcionesController@emailresetpass');
Route::post('updateresetpass', 'InscripcionesController@updateresetpass');

Route::group(['prefix' =>'admin'],function(){

    Route::get('/',[
        'uses'=>'Admin\LoginController@index',
        'as'=>'admin.login'
    ]);

    Route::post('log', 'Admin\LoginController@log');

    Route::group(['middleware' => ['auth']],function(){

        Route::get('/indexadmin', function () {
            return view('admin.index');
        });


        Route::post('logout', 'Admin\LoginController@logout');

        Route::get('index', 'Admin\LoginController@template');

        Route::get('user', [
            'uses'=>'Admin\UserController@index',
            'as'=>'admin.user'
        ]);
/*
Route::get('admin/store/category/create/{idTienda}', 'Admin\StoreController@categoryCreate');
Route::post('admin/store/category/created', 'Admin\StoreController@categoryCreated');

Route::post('admin/store/product/created', 'Admin\StoreController@productCreated');
Route::get('admin/store/product/create/{idCategoria}', 'Admin\StoreController@productCreate');
Route::get('admin/store/product/delete/{id}', 'Admin\StoreController@productDelete');
Route::get('admin/store/product/edit/{id}', 'Admin\StoreController@productEdit');
Route::post('admin/store/product/edited', 'Admin\StoreController@productEdited');*/

        Route::get('edit/{id}', 'Admin\UserController@edit');
        Route::get('delete/{id}', 'Admin\UserController@delete');
        Route::get('create', 'Admin\UserController@create');
        Route::post('user/changepass', 'Admin\UserController@changePass');
        Route::get('edit_store/{id}', 'Admin\UserController@editartienda');
        Route::post('user/created', 'Admin\UserController@store');
        Route::post('user/edited', 'Admin\UserController@update');
        //Route::post('user/editedtienda', 'Admin\UserController@updatetienda');

        Route::get('store/create', [
            'uses'=>'Admin\StoreController@create',
            'as'=>'admin.store.create'
        ]);

        Route::get('store/{idt?}', 'Admin\StoreController@index');
        Route::get('store/create', 'Admin\StoreController@create');
        Route::post('store/create', 'Admin\StoreController@store');
        Route::get('store/delete/{id}', 'Admin\StoreController@delete');
        Route::get('store/propietario/{id}',[
            'uses'=>'Admin\StoreController@propietario',
            'as'=>'admin.store.edipropie'
        ]);
        
        Route::post('store/updatepropietario', 'Admin\StoreController@updatepropietario');
        Route::get('catego/{id}', 'Admin\CategoryController@categoriaportiendas');
        Route::get('categoid/{id}', 'Admin\CategoryController@category');
        Route::get('catego/delete/{id}', 'Admin\CategoryController@delete');
        Route::post('catego/update', 'Admin\CategoryController@update');

        Route::get('categ/crear', 'Admin\CategoryController@create');
        Route::post('categoria/crear', 'Admin\CategoryController@store');

        Route::get('product', 'Admin\ProductController@index');
        Route::get('product/{tienda}/{categoria}', 'Admin\ProductController@productporcategoria');
        Route::get('product/create/{tienda}/{categoria}', 'Admin\ProductController@create');
        Route::post('product/create', 'Admin\ProductController@store');
        Route::get('product/productcatego/edit/{tienda}/{categoria}/{producto}', 'Admin\ProductController@edit');
        Route::post('product/update', 'Admin\ProductController@update');
        Route::get('product/productcatego/delete/{tienda}/{categoria}/{producto}', 'Admin\ProductController@delete');
    });
});
//Route::post('admin/edit', 'Admin\UserController@edit');

// Route::get('admin/index', function () {

Auth::routes();
Route::get('/error',function() {
    abort(601) ;
});
// Route::get('/home', 'HomeController@index')->name('home');