<img src="{{$message->embed(asset('img/logo.png'))}}">
<div class="container">
    <h2 class="accent-1">Peticion de cambio de contraseña</h2>
    <label>Nombre:</label>
    {{$nombre}}
    <br>
    <label>Correo:</label>
    {{$correo}}
    <br>
    <p> Usted solicito un cambio de contraseña si no es asi ignore este correo!</p>
    <br>
    <div style="margin: 0 auto; height: auto; width: 250px; cursor: pointer">
        <a href="{{url('/formresetpass',["id"=>$id])}}" style="margin:0 auto;">
            <button type="button" style="margin:0 auto; background-color: #41B9E6;height: 60px;width: 250px;border:none;color:white;font-family: 'Arial';font-size: 15px;margin-top: 10px; margin-bottom: 10px">
                Cambiar contraseña
            </button>
        </a>
    </div>
</div>