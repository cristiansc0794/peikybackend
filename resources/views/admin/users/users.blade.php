@extends('template.admin.contenido')

@section('contenido_cliente')
    <br>

    <div class="col-md-12">
        <article class="margin-bottom-xxl">
            <h2 class="text-primary"><strong>Usuarios en el sistema</strong></h2>
        </article>
    </div>

    <div class="container-fluid">
        <br><br>
     <div class="row">
         <div class="col-lg-12">
             <div class="card">
                 <div class="card-body">
                    <div class="table-responsive">
                     <table class="table table-striped no-margin table-bordered" id="TableUser">
                         <thead>
                         <tr>
                             <th>Id</th>
                             <th>Nombre</th>
                             <th>Email</th>
                             <th>Telefono</th>
                             <th>Estado</th>
                             <th>Tipo usuario</th>
                             <th>Tienda</th>
                             <th>Tipo de servicio</th>
                             <th>Opciones</th>
                         </tr>
                         </thead>
                         <tbody>
                         @foreach ($users as $user)
                             <tr>
                                 <td>{{ $user->id }}</td>
                                 <td>{{ $user->name }}</td>
                                 <td>{{ $user->email }}</td>
                                 <td>{{ $user->phone }}</td>
                                 <td>
                                    @if($user->state==1)
                                         <p>Activo</p>
                                     @else
                                        <p>Inactivo</p>
                                     @endif
                                 </td>
                                 <td>{{ $user->type_users }}</td>
                                 <td>{{ $user->tienda }}</td>
                                 <td>

                                    @if($user->typeofservice=="1")
                                    Restriccion por producto
                                    @else
                                    Premium
                                    @endif
                                 </td>
                                 <td>
                                     <a class="btn btn-info" data-toggle="tooltip" title="Editar" href="{{url('admin/edit',[$user->id]) }}" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                     @if($user->state==1)
                                     <a class="btn btn-warning" data-toggle="tooltip" title="Eliminar usuario!" href="{{url('admin/delete',[$user->id]) }}" role="button"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                     @else
                                         <a class="btn btn-success" data-toggle="tooltip" title="Dar de alta" href="{{url('admin/delete',[$user->id]) }}" role="button"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a>
                                     @endif
                                 </td>
                             </tr>
                         @endforeach
                        @if(isset($usuarioN))
                         @foreach($usuarioN as $user)
                             <tr>
                                 <td>{{ $user->id }}</td>
                                 <td>{{ $user->name }}</td>
                                 <td>{{ $user->email }}</td>
                                 <td>{{ $user->phone }}</td>
                                 <td>
                                    @if($user->state==1)
                                         <p>Activo</p>
                                     @else
                                        <p>Inactivo</p>
                                     @endif
                                 </td>
                                 <td>{{ $user->type_users }}</td>
                                 <td>Tienda no asignada</td>
                                 <td>

                                    @if($user->typeofservice=="1")
                                    Restriccion por producto
                                    @else
                                    Premium
                                    @endif
                                 </td>

                                 <td><a class="btn btn-info" data-toggle="tooltip" title="Editar" href="edit/{{ $user->id }}" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                     @if($user->state=="1")
                                         <a class="btn btn-warning" data-toggle="tooltip" title="Eliminar" href="{{url('admin/delete',[$user->id]) }}" role="button"><i class="fa fa-trash-o" aria-hidden="true"></i></a>

                                     @else
                                         <a class="btn btn-success" data-toggle="tooltip" title="Dar de alta" href="{{url('admin/delete',[$user->id]) }}" role="button"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a>
                                     @endif

                                 </td>
                             </tr>
                         @endforeach
                         @endif
                         </tbody>
                     </table>
                        <a class="btn btn-info" href="create" role="button">Crear nuevo usuario</a>
                     </div>
                 </div>

             </div>
         </div>
     </div>
    </div>
    <script src="{{asset('js/Admin.js')}}"></script>
    <script src="{{asset('js/user.js')}}"></script>

@endsection