
@extends('template.admin.contenido')

@section('contenido_cliente')
  <br>
  <div class="container">
    <div class="row">
      <div class="card">
        <div class="card-head style-primary">
          <header>
            <h3>Editar usuario</h3>
          </header>
        </div>
        <div class="card-body">
          <form class="form-horizontal" role="form" method="POST" action="../user/edited">
                {{ csrf_field() }}
                <input name="id" type="hidden" value="{{$user->id}}">

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name">Nombre:</label>
                <input name="name" type="text" class="form-control" id="name" value="{{$user->name}}">
              </div>

              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email">Email:</label>
                <input name="email" type="text" class="form-control" id="email" value="{{$user->email}}">
              </div>

            <div class="form-group">
              <label for="type_users">Tipo Usuario:</label>
              <select id="type_users" name="type_users" class="form-control">
                @if($user->type_users=="admin")
                  <option value="admin" selected>administrador</option>
                  <option value="provider">proveedor</option>
                @else
                  <option value="provider" selected>proveedor</option>
                  <option value="admin">administrador</option>
                @endif
              </select>
            </div>

            <div class="form-group">
              <label for="type_users">Estado:</label>
              <select id="type_users" name="state" class="form-control">
                @if($user->state==1)
                  <option value="1" selected>Activo</option>
                  <option value="0">Inactivo</option>
                @else
                  <option value="1">Activo</option>
                  <option value="0" selected>Inactivo</option>
                @endif
              </select>
            </div>

            <div class="form-group">
              <label for="typeofservice">Tipo de srvicio:</label>
              <select id="typeofservice" name="typeofservice" class="form-control">
                @if($user->typeofservice==1)
                  <option value="1" selected>Restriccion por producto</option>
                  <option value="2">Premium</option>
                @else
                  <option value="2" selected>Premium</option>
                  <option value="1">Restriccion por producto</option>
                @endif
              </select>
            </div>

            <div class="form-group">
              <label for="phone">Telefono:</label>
              <input name="phone" type="tel" class="form-control" id="phone" value="{{$user->phone}}">
            </div>

              <button type="submit" class="btn btn-info">Editar</button>
              <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Cambiar Contraseña</button>
              <a class="btn btn-danger" href="{{url('admin/user')}}">Cancelar</a>

          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title ">Cambiar contraseña</h4>
            </div>
            <div class="modal-body">

              <form class="form-horizontal" role="form" method="POST" action="../user/changepass">
                <input name="id" type="hidden" value="{{$user->id}}">



                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                  <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                      <label for="password_confirmation" class="col-md-4 control-label">Password Confirm</label>

                      <div class="col-md-6">
                          <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required>

                          @if ($errors->has('password_confirmation'))
                              <span class="help-block">
                                 <strong style="margin-bottom: 50px">{{ $errors->first('password_confirmation') }}</strong>
                                 <br>
                               </span>
                          @endif
                      </div>
                  </div>

                <div class="modal-footer">
                  <button type="submit" class="btn btn-default">Guardar</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
              </form>

            </div>
        </div>

    </div>
</div>
  @if($errors->has('password_confirmation'))
      <script type="text/javascript">
          $('#myModal').modal('show');
      </script>
  @endif
@endsection()
