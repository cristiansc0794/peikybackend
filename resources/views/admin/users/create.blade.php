
@extends('template.admin.contenido')

@section('contenido_cliente')
    <br>
    <div class="container">
        <div class="row">
                <div class="card">
                    <div class="card-head style-primary">
                        <header>
                           <h3>Registre al usuario</h3>
                        </header>
                    </div>
                    <div class="card-body">
                        <form class="form" role="form" method="POST" action="{{url('admin/user/created') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} floating-label">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                <label for="name">Nombre</label>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} floating-label">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                <label for="email">Email</label>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} floating-label">
                                <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}" required autofocus>
                                <label for="password">Password</label>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : '' }} floating-label">
                                <input id="password-confirm" type="password" class="form-control" name="password-confirm" value="{{ old('password-confirm') }}" required autofocus>
                                <label for="password-confirm">Confirmar Password</label>
                                @if ($errors->has('password-confirm'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('password-confirm') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} floating-label">

                                <input id="phone" type="tel" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>
                                <label for="phone">Teléfono</label>
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                    <a class="btn btn-danger" href="{{url('admin/user')}}">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div><!--end .card-body -->
                </div><!--end .card -->
                <em class="text-caption">​Grimorum.ltda</em>
            </div>
        </div>

@endsection()