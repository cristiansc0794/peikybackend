
@extends('template.admin.contenido')

@section('contenido_cliente')
    <br>
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-head style-primary">
                    <header>
                        <h3>Tienda del usuario: {{$tienda->nameU}}</h3>
                    </header>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{url('admin/user/editedtienda') }}">
                    {{ csrf_field() }}
                        <input name="id" type="hidden" value="{{$tienda->id}}">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">Nombre de la tienda:</label>
                            <input name="name" type="text" class="form-control" id="name" value="{{$tienda->nameS}}">
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone">Telefono de la tienda:</label>
                            <input name="phone" type="tel" class="form-control" id="phone" value="{{$tienda->phone}}">
                        </div>

                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address">Dirección de la tienda:</label>
                            <input name="address" type="text" class="form-control" id="address" value="{{$tienda->address}}">
                        </div>

                        <button type="submit" class="btn btn-info">Editar</button>
                        <a class="btn btn-danger" href="{{url('admin/user')}}">Cancelar</a>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection