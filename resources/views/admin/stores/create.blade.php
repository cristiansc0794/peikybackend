
@extends('template.admin.contenido')

@section('contenido_cliente')
    <br>
    <div class="container">
        <div class="row">
            @if(Session::has('error'))
                <br>
                <div class="alert alert-danger" role="alert">
                    <strong>{{Session::get('error')}}</strong>
                </div>
            @endif
            <div class="card">
                <div class="card-head style-primary">
                    <header>
                        <h3>Registro de tiendas</h3>
                    </header>
                </div>
                <div class="card-body">
                    <form class="form" role="form" method="POST" action="{{url('admin/store/create') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} floating-label">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                            <label for="name">Nombre</label>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} floating-label">

                            <input id="phone" type="tel" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>
                            <label for="phone">Teléfono</label>
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }} floating-label">
                            <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required autofocus>
                            <label for="password">Direccion</label>
                            @if ($errors->has('address'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="id_users">Usuarios disponibles:</label>
                            <select id="id_users" name="id_users" class="form-control">
                                <option value="">Seleccione un usuario</option>
                                @foreach($users as $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                 @endforeach
                            </select>
                        </div>

                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    Registrar
                                </button>
                                <a class="btn btn-danger" href="{{url('admin/store')}}">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div><!--end .card-body -->
            </div><!--end .card -->
            <em class="text-caption">​Grimorum.ltda</em>
        </div>
    </div>
@endsection()