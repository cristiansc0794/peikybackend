@extends('template.admin.contenido')

@section('contenido_cliente')
<div class="container">
<h1>{{$category->name}}</h1> <br>

{{$category->logo}}<br>

  <table class="table">
    <thead>
      <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>Descripción</th>
        <th>Precio</th>
        <th>Imagen</th>
        <th>Cantidad</th>
      </tr>
    </thead>
    <tbody>
    @foreach ($products as $product)
      <tr>
        <td>{{$product->id}}</td>
        <td>{{$product->name}}</td>
        <td>{{$product->description}}</td>
        <td>{{$product->precio}}</td>
        <td>{{$product->imagen}}</td>
        <td>{{$product->cantidad}}</td>
        <td><a class="btn btn-primary" href="../product/edit/{{$product->id}}" role="button">Editar</a></td>
        <td><a class="btn btn-danger" href="../product/delete/{{$product->id}}" role="button">Eliminar</a></td>
      </tr>
    @endforeach
    </tbody>
  </table>
  <a class="btn btn-primary" href="../product/create/{{$category->id}}" role="button">Crear un nuevo producto</a>
</div>

@endsection