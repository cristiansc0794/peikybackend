
@extends('template.admin.contenido')

@section('contenido_cliente')
    <br>
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-head style-primary">
                    <header>
                        <h3>Editar Producto</h3>
                    </header>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{url('admin/product/update') }}" enctype="multipart/form-data" files="true">
                        {{ csrf_field() }}
                        <input name="id" type="hidden" value="{{$product->id}}">
                        <input name="idtiendap" type="hidden" value="{{$product->idtiendap}}">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">Nombre del producto:</label>
                            <input name="name" type="text" class="form-control" id="name" value="{{$product->name}}">
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">Derscripción:</label>
                            <input name="description" type="text" class="form-control" id="name" value="{{$product->description}}">
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">Precio:</label>
                            <input name="precio" type="text" class="form-control" id="name" value="{{$product->precio}}">
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="imagen">Imagen:</label>
                            <input type="file" name="imagen"  class="form-control" id="imagen">
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">Cantidad:</label>
                            <input name="cantidad" type="text" class="form-control" id="name" value="{{$product->cantidad}}">
                        </div>

                        <div class="form-group">
                          <label for="type_users">Categoría:</label>
                          <select id="type_users" name="id_category" class="form-control">

                            @foreach ($categories as $category)

                                @if($category->id==$product->id_category)
                                  <option value="{{$category->id}}" selected>{{$category->name}}</option>
                                @else
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endif

                            @endforeach
                              
                          </select>
                        </div>
                        <button type="submit" class="btn btn-info">Editar</button>
                        <a class="btn btn-danger" href="{{url('admin/product',[$product->idtiendap,$product->id_category])}}">Cancelar</a>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection