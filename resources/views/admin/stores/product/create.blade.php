
@extends('template.admin.contenido')

@section('contenido_cliente')
    <br>
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-head style-primary">
                    <header>
                        <h3>Crear Producto</h3>
                    </header>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{url('admin/product/create') }}" enctype="multipart/form-data" files="true">
                        {{ csrf_field() }}
                        <input name="id_category" type="hidden" value="{{$categoria}}">
                        <input name="id_tienda" type="hidden" value="{{$tienda}}">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">Nombre del producto:</label>
                            <input name="name" type="text" class="form-control" id="name">
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description">Derscripción:</label>
                            <input name="description" type="text" class="form-control" id="description">
                        </div>

                        <div class="form-group{{ $errors->has('precio') ? ' has-error' : '' }}">
                            <label for="precio">Precio:</label>
                            <input name="precio" type="text" class="form-control" id="precio">
                        </div>

                        <div class="form-group{{ $errors->has('imagen') ? ' has-error' : '' }}">
                            <label for="imagen">Imagen:</label>
                            <input type="file" name="imagen"  class="form-control" id="imagen">
                        </div>

                        <div class="form-group{{ $errors->has('cantidad') ? ' has-error' : '' }}">
                            <label for="cantidad">Cantidad:</label>
                            <input name="cantidad" type="text" class="form-control" id="cantidad">
                        </div>

                        <button type="submit" class="btn btn-info">Guardar</button>
                        <a class="btn btn-danger" href="{{url('admin/product/'.$tienda.'/'.$categoria)}}">Cancelar</a>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection