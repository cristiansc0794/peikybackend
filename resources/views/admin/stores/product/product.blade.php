@extends('template.admin.contenido')

@section('contenido_cliente')
<br>

    <div class="col-md-12">
        <article class="margin-bottom-xxl">
                <h2 class="text-primary"> Tienda: <strong>{{$nombretienda}}</strong></h2>
                <h4><em>Productos de la categoria:</em> <strong>{{$nombrecatego}}</strong></h4>
        </article>
    </div>

    <div class="container-fluid">
        <br>
     <div class="row">
         <div class="col-lg-12">
             <div class="card">
                 <div class="card-body">
                    <div class="table-responsive">
                         <table class="table table-bordered" id="TableProduct">
                             <thead>
                             <tr>
                                 <th>Id</th>
                                 <th>Imagen</th>
                                 <th>Producto</th>
                                 <th>Descripción</th>
                                 <th>Precio</th>
                                 <th>Cantidad</th>
                                 <th>Opciones</th>
                             </tr>
                             </thead>
                             <tbody>
                             @if(count($products)!=0)
                             @foreach ($products as $product)
    							<tr>
    							 <td>{{ $product->id }}</td>
                                 <td>
                                     <img src="{{asset('imagenes/productos/'.$product->imagen)}}" alt="{{$product->name}}" height="100px" width="100px" class="img-thumbnail">
                                 </td>
    							 <td>{{ $product->name }}</td>
    							 <td>{{ $product->description }}</td>
    							 <td>{{ $product->precio }}</td>
    							 <td>{{ $product->cantidad }}</td>
    							 <td>
                                 <a href="{{url('admin/product/productcatego/edit',[$tienda,$categoria,$product->id])}}" data-toggle="tooltip" title="Editar" class="btn btn-info"><span class="title"><i class="fa fa-pencil" aria-hidden="true"></i></span></a>
    							 <a class="btn btn-danger" href="{{url('admin/product/productcatego/delete',[$tienda,$categoria,$product->id]) }}" data-toggle="tooltip" title="Eliminar" role="button"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
    							 </td>
    							</tr>
                             @endforeach
                             @endif
                             </tbody>
                         </table>
                     </div>
                     <a class="btn btn-info" href="{{url('admin/product/create',[$tienda,$categoria])}}" role="button">Crear nuevo Producto</a>
                     <a class="btn btn-warning" href="{{url('admin/store',[$tienda])}}" role="button">Ir atras</a>
                 </div>

             </div>
         </div>
     </div>
    </div>
    <script src="{{asset('js/Admin.js')}}"></script>
    <script src="{{asset('js/product.js')}}"></script>
@endsection