
@extends('template.admin.contenido')

@section('contenido_cliente')
    <br>
    <div class="container">
        <div class="row">
            
            <div class="card">
                <div class="card-head style-primary">
                    <header>
                        <h3>Tienda: {{$store->name}}</h3>
                    </header>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{url('admin/store/updatepropietario') }}">
                        {{ csrf_field() }}
                        <input name="id" type="hidden" value="{{$store->id}}">
                        <div class="form-group">
                            <label for="name">Nombre de la tienda:</label>
                            <input name="name" type="text" class="form-control" id="name" value="{{$store->name}}">
                        </div>

                        <div class="form-group">
                            <label for="username">Dueño de la tienda actualmente:</label>
                            <input name="username" type="text" class="form-control" id="username" value="{{$store->username}}">
                        </div>

                        <div class="form-group">
                            <label for="phone">Telefono de la tienda:</label>
                            <input name="phone" type="tel" class="form-control" id="phone" value="{{$store->phone}}">
                        </div>

                        <div class="form-group">
                            <label for="address">Dirección de la tienda:</label>
                            <input name="address" type="text" class="form-control" id="address" value="{{$store->address}}">
                        </div>


                      <div class="form-group">
                          <label for="type_users">Usuario disponibles para el cambio:</label>
                          <select id="type_users" name="id_users" class="form-control">
                              <option value="">Usuarios Disponibles</option>
                            @foreach ($users as $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                              
                          </select>
                        </div>





                        






                        <button type="submit" class="btn btn-info">Editar</button>
                        <a class="btn btn-danger" href="{{url('admin/store')}}">Cancelar</a>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection