@extends('template.admin.contenido')

@section('contenido_cliente')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.css"/>

<br>

    <div class="col-md-12">
        <article class="margin-bottom-xxl">
            <h2 class="text-primary"><strong>Tiendas en el sistema</strong></h2>
        </article>
    </div>

    <div class="container-fluid">
        <br><br>
     <div class="row">
         <div class="col-lg-12">
             <div class="card">
                 <div class="card-body">
                    <div class="table-responsive">
<!--                    <a class="btn btn-info" href="create" role="button">Crear nuevo usuario</a>-->
                     <table class="table table-striped no-margin table-responsive table-bordered" id="TableProduct">
                         <thead>
                         <tr>
                             <th>Id</th>
                             <th>Nombre</th>
                             <th>Telefono</th>
                             <th>Dirección</th>
                             <th>Propietario</th>
                             <th>Opciones</th>
                         </tr>
                         </thead>
                         <tbody>
                         @if(isset($stores))
                         @foreach ($stores as $store)
                             <tr>
                                 <td>{{ $store->id }}</td>
                                 <td id="namestore{{ $store->id }}">{{ $store->name }}</td>
                                 <td>{{ $store->phone }}</td>
                                 <td>{{ $store->address }}</td>
                                 <td>{{ $store->username }}</td>
                                 <td>

                                    <a href="{{url('admin/store/propietario',[$store->id])}}" data-toggle="tooltip" title="Editar tienda" class="btn btn-info"><span class="title"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a>
                                     <a type="button" class="btn btn-warning" data-toggle="tooltip" title="Ver categorías de la tienda" onclick="cargardatos({{$store->id}})" id="{{$store->id}}" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                                     @if($store->state==1)
                                         <a class="btn btn-danger" data-toggle="tooltip" title="Eliminar tienda" href="{{url('admin/store/delete',[$store->id]) }}" role="button"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                     @else
                                     <a class="btn btn-success" href="{{url('admin/store/delete',[$store->id]) }}" data-toggle="tooltip" title="Dar de alta"  role="button"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a>
                                     @endif
                                 </td>
                             </tr>
                         @endforeach
                         @endif
                         </tbody>
                     </table>
                     </div>
                     <a class="btn btn-info" href="{{url('admin/store/create')}}" role="button">Crear nuevo Tienda</a>
                 </div>

             </div>
         </div>
     </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal list cateogy-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="tittlelistcat"></h4>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="modal-body">
                            <div class="table-responsive">
                                <table id="tablaA" class="table  table-responsive  ">
                                <thead>
                                <tr>
                                    <th>Categorias</th>
                                    <th>Logo</th>
                                    <th>Opciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                             </div>
                        </div>
                        <div class="modal-footer" id="footermodalid">
                        </div>
                    </div>
                 </div>
            </div>
        </div>
</div>


<!-- Modal create category -->
<div class="modal fade" id="myModalcreatecat" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Crear Categoria</h4>
            </div>
            <form class="form-horizontal" id="Registrocatego" enctype="multipart/form-data" files="true">
                <div class="modal-body">
                        <table id="" class="table  table-responsive  ">
                            {{ csrf_field() }}
                            <tbody>
                                <tr>
                                    <div class="form-group" id="idtiendahidden"></div>
                                </tr>
                                <tr>
                                    <div id="divnamecat" class="form-group">
                                        <label for="name">Nombre de la categoria:</label>
                                        <input name="name" type="text" class="form-control" id="name">
                                    </div>
                                    <em class="text-caption" id="divnamecaterror"></em>
                                </tr>
                                 <tr>
                                     <div id="divlogocat" class="form-group">
                                         <label for="logo">Logo:</label>
                                         <input type="file" name="logo"  class="form-control" id="logo">
                                     </div>
                                     <em class="text-caption" id="divlogocaterror"></em>
                                 </tr>
                            </tbody>
                        </table>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" id="enviarcatego">Crear</button>
                    <button type="button" class="btn btn-danger" id="cancelcatego">Cancelar</button>
                </div>
            </form>
        </div>

    </div>
</div>



<!-- Modal create category -->
<div class="modal fade" id="modalEditCat" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title ">Editar Categoria</h4>
            </div>
            <form class="form-horizontal" id="formUpdateCatego" enctype="multipart/form-data" files="true" name="formUpdateCatego">
                <div class="modal-body">
                    {{ csrf_field() }}

                    <input type="hidden" class="form-control" id="idcat" name="idcat">
                    <input type="hidden" class="form-control" id="idcat" name="idstore">
       
                    <div id="divnamecatedit" class="form-group">
                        <label for="Nombre">Nombre:</label>
                        <input type="text" class="form-control" id="namecat" name="namecat">
                    </div>
                    <em class="text-caption" id="emnamecatedit"></em>

                    <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                        <label for="logo">Logo:</label>
                        <input type="file" name="logocat"  class="form-control" id="logocat">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="updatecatego">Guardar</button>
                    <button type="button" class="btn btn-danger" id="btncancel_edt">Cancelar</button>
                </div>
            </form>
        </div>

    </div>
</div>
<script src="{{asset('js/Admin.js')}}"></script>
<script src="{{asset('js/product.js')}}"></script>
@if(isset($idtc))
    <script>
        cargardatos(<?= $idtc ?>);
    </script>
@endif
@endsection