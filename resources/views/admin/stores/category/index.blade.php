@extends('template.admin.contenido')

@section('contenido_cliente')
    <br>

    <div class="col-md-12">
        <article class="margin-bottom-xxl">
            <h1 class="text-primary">Tiendas en el sistema</h1>
        </article>
    </div>

    <div class="col-lg-8">
        <article class="margin-bottom-xxl">
            <p class="lead">
            </p>
        </article>
    </div>

    <div class="col-md-8">
        <article class="margin-bottom-xxl">
            <p>
                Create responsive tables by wrapping any <code>.table</code> in <code>.table-responsive</code> to make them scroll horizontally up to small devices (under 768px).
                When viewing on anything larger than 768px wide, you will not see any difference in these tables.
            </p>
        </article>
    </div>
    <div class="container-fluid">
        <br><br>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <!--                    <a class="btn btn-info" href="create" role="button">Crear nuevo usuario</a>-->
                        <table class="table table-striped no-margin table-responsive ">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Telefono</th>
                                <th>Dirección</th>
                                <th>Propietario</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($stores))
                            @foreach ($stores as $store)
                                <tr>
                                    <td>{{ $store->id }}</td>
                                    <td>{{ $store->name }}</td>
                                    <td>{{ $store->phone }}</td>
                                    <td>{{ $store->address }}</td>
                                    <td>{{ $store->username }}</td>
                                    <td>
                                        <a class="btn btn-warning" href="{{url('admin/store/delete',[$store->id]) }}" role="button">Eliminar</a>
                                        <a href="{{url('admin/store/propietario',[$store->id])}}" class="btn btn-info"><span class="title">Cambiar propietario</span></a>
                                        <a class="btn btn-danger" href="{{url('admin/edit_store', [$store->userid]) }}" role="button">Editar tienda</a>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('js/Admin.js')}}"></script>
@endsection