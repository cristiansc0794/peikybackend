
@extends('template.admin.contenido')

@section('contenido_cliente')
    <br>
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-head style-primary">
                    <header>
                        <h3>Crear Categoria</h3>
                    </header>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{url('admin/categoria/crear') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">Nombre de la categoria:</label>
                            <input name="name" type="text" class="form-control" id="name">
                        </div>

                        <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                            <label for="logo">Logo:</label>
                            <input name="logo" type="text" class="form-control" id="logo">
                        </div>

                        <div class="form-group">
                            <label for="id_store">Tiendas disponible:</label>
                            <select id="id_store" name="id_store" class="form-control">
                                <option value="">Seleccione una tienda</option>
                                @foreach($tiendas as $tie)
                                    <option value="{{$tie->id}}">{{$tie->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <button type="submit" class="btn btn-info">Guardar</button>
                        <a class="btn btn-danger" href="{{url('admin/store')}}">Cancelar</a>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection