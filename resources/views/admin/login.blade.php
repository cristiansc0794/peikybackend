<!DOCTYPE html>
<html lang="en">

<head>
    <!-- <title>Material Admin - Login</title> -->

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('css/materialadmin.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('css/material-design-iconic-font.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('css/parallax.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('css/estilos.css')}}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/css?family=Great+Vibes|Josefin+Slab:300" rel="stylesheet">

</head>

<body class="menubar-hoverable header-fixed">

    <!-- parallax init-->
    <!--<div class="parallax-container">
    <div class="parallax"><img src="" style="height:465px;margin-top:40px;display: block; transform: translate3d(-100%, 0px, 0px);"></div>
</div>-->
<br>
    <!-- BEGIN LOGIN SECTION -->
    <div class="section log">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-xs-offset-0 col-md-6 col-md-offset-3">
                    <center><img class="imgLogo" src="{{asset('img/logo.png')}}" alt=""></center>

                    <!--<p class="text-center Titulo">Peiky</p>-->
                    

                    @if(Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{Session::get('error')}}
                    </div>
                    @endif

                    <form method="post" action="{{url('admin/log') }}" accept-charset="utf-8">
                        <div class="z-depth-1 grey lighten-4" style="padding: 50px 48px 30px 48px; border: 1px solid #EEE;">
                            <h5 class="text-center parrafo">Inicia sesión</h5>
                            {{ csrf_field() }}
                            <div class='row'>
                                <div class='input-field col s12'>
                                    <input type="email" class="form-control" id="username" name="email" value="{{ old('email') }}" required autofocus>
                                    <label for='email'>Enter your email</label>
                                </div>
                            </div>
                            <div class='row'>
                                <div class='input-field col s12'>
                                    <input type="password" class="form-control" id="password" name="password" required>
                                    <label for='password'>Enter your password</label>
                                </div>
                                <label style='float: right;'>
                                    <a class='pink-text' href="{{url('/recuperarpass')}}"><b>Olvide mi contraseña?</b></a>
                                </label>
                            </div>

                            <div class='row'>
                                <button type='submit' name='btn_login' class='btn btn-block btn_login #311b92 deep-purple darken-4'>Login</button>
                            </div>

                        </div>

                    </form>

                </div>


            </div>
        </div>
    </div>

    <!-- END LOGIN SECTION -->

    <!-- parallax end -->
    <!-- <div class="parallax-container" style="height: 370px;">
        <div class="parallax"><img src="" style="display: block; transform: translate3d(-100%, 0px, 0px);"></div>
    </div>-->

    <!-- BEGIN JAVASCRIPT -->
    <script src="{{asset('js/libs/jquery/jquery-1.11.2.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
    <script src="{{asset('js/libs/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/parallax.js')}}"></script>

    <!-- END JAVASCRIPT -->

</body>

</html>