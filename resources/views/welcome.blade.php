@extends('layouts.app') 

@section('contenido')

		<section id="portada">
			<div class="container">
					<div class="row">
						<div class="col s12 l8 message-portada">
							<p>
								<span>Agiliza tus procesos de compra</span><br>
								y sigue ofreciendo un buen servicio a tus <br>
								clientes gracias a <span>Peiky</span>
							</p>
					</div>
				</div>
			</div>
			<img src="{{asset('img/phone.png')}}" alt="iPhone app" class="hide-on-med-and-down">
			<a href="#content">
				<div class="arrow-down valign-wrapper z-depth-3">
					<i class="fa fa-angle-down" aria-hidden="true"></i>
				</div>
			</a>
		</section>

		<section id="content" class="scrollspy">
			<div class="container">
				<div class="row">
					<div class="col s8 offset-s2 m4 hide-on-large-only">
							<img src="{{asset('img/phone.png')}}" alt="iPhone app" class="responsive-img">
					</div>
					<div class="col s12 m6 offset-m1 l6  init ">
						<p>
							Peiky te ofrece la posibilidad de agilizar todos tus procesos
							de ventas en redes sociales, comunícate más rápido con tus
							clientes y cierra compras sin dejar esperando a tus clientes;
							resuelve sus dudas por medio de un teclado inteligente apto
							para todas las redes que manejes.
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m8 offset-m2 l6 ">
						<div class="card horizontal tarjeta" id="asd">
				      <div class="card-image">
				        <img src="{{asset('img/square1.png')}}" class="responsive-img">
				      </div>
				      <div class="card-stacked">
				        <div class="card-content">
				        	<h3>Entrega de estadísticas</h3>
				          <p>¿Cuánto vendiste este mes?<br>
										Información rápida y fácil de <br>
										entender en <span>Peiky</span>.
									</p>
				        </div>
				      </div>
				    </div>
					</div>
					<div class="col s12 m8 offset-m2 l6">
						<div class="card horizontal tarjeta">
				      <div class="card-image">
				        <img src="{{asset('img/square2.png')}}" class="responsive-img">
				      </div>
				      <div class="card-stacked">
				        <div class="card-content">
				        	<h3>Manejo de inventario</h3>
				          <p><span>No más Excel</span> para manejar tu <br>
										inventario, controla tu inventario <br>
										en tiempo real.
									</p>
				        </div>
				      </div>
				    </div>
					</div>
				</div>
				<div class="row">
					<div class="col s10 l7 form center-align">
						<h2 class="title_form">¿Tienes alg<span>una duda?</span></h2>
						<span class="title_span">Inscríbete y con gusto te contactaremos</span>
						<form method="post" action="{{url('/inscripciones')}}" accept-charset="utf-8">
							  <div class="row">
								<div class="input-field col s12">
									<i class="material-icons prefix">account_circle</i>
								  <input id="name" type="text" class="validate" name="name">
								  <label for="name">Nombre completo</label>
								</div>
								  @if ($errors->has('name'))
									  <span class="help-block">
                                    	<strong>{{ $errors->first('name') }}</strong>
                                	  </span>
								  @endif
							  </div>
							  <div class="row">
								<div class="input-field col s12">
									<i class="material-icons prefix">email</i>
								  <input id="email" type="email" class="validate" name="email">
								  <label for="email" data-error="Ingrese un correo válido">Correo electrónico</label>
								</div>
								  @if ($errors->has('email'))
									  <span class="help-block">
                                    	<strong>{{ $errors->first('email') }}</strong>
                                	  </span>
								  @endif
							  </div>
							  <div class="row">
								<div class="input-field col s12">
									<i class="material-icons prefix">phone</i>
								  <input id="phone" type="tel" class="validate" name="phone">
								  <label for="phone">Teléfono</label>
								</div>
								  @if ($errors->has('phone'))
									  <span class="help-block">
                                    	<strong>{{ $errors->first('phone') }}</strong>
                                	  </span>
								  @endif
							  </div>
							  <div class="row center-align">
								<button class="btn waves-effect waves-light" type="submit">Enviar</button>
							  </div>
				    </form>
					</div>
				</div>
			</div>
		</section>

		<footer class="page-footer grey darken-3">
		  <div class="container">
		    <div class="row">
		      <div class="col s12 center-align">
		        <h5 class="white-text">Visítanos</h5>
		        <div class="social_media">
		        	<a>
								<div class="social valign-wrapper z-depth-2 white waves-effect" onclick="Materialize.toast('¡Redes sociales muy pronto!', 2000)">
									<i class="fa fa-facebook grey-text text-darken-3" aria-hidden="true"></i>
								</div>
							</a>
							<a>
								<div class="social valign-wrapper z-depth-2 white waves-effect" onclick="Materialize.toast('¡Redes sociales muy pronto!', 2000)">
									<i class="fa fa-instagram grey-text text-darken-3" aria-hidden="true"></i>
								</div>
							</a>
		        </div>
		      </div>
		    </div>
		  </div>
		  <div class="footer-copyright">
		    <div class="container center-align">
		    	<a href="http://www.grimorum.com.co/" class="grey-text text-lighten-1">© 2017 Grimorum, Bogotá D.C, Colombia. Esta es una idea corazón naranja desarrollada por Grimorum <i class="fa fa-heart" aria-hidden="true"></i></a>
		    </div>
		  </div>
		</footer>
@endsection
