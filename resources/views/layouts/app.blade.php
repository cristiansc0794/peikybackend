<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>LANDING PEYKI</title>
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
        <link type="text/css" rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" />
        <link rel="stylesheet" href="{{asset('css/general.css')}}">
    </head>
    <body>

        <header>
         <nav class="white">
            <div class="nav-wrapper valign-wrapper">
              <a href="/" class="brand-logo "><img class="responsive-img" src="{{asset('img/logo.png')}}" alt="Logo Peiky"></a>
                    
                <!-- MENÚ DE CONTENIDO -->

              <!-- <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
              <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="sass.html">Sass</a></li>
                <li><a href="badges.html">Components</a></li>
                <li><a href="collapsible.html">JavaScript</a></li>
              </ul>
              <ul class="side-nav" id="mobile-demo">
                <li><a href="sass.html">Sass</a></li>
                <li><a href="badges.html">Components</a></li>
                <li><a href="collapsible.html">Javascript</a></li>
                <li><a href="mobile.html">Mobile</a></li>
              </ul> -->
            </div>
          </nav>
        </header>

        <main>
            @yield('contenido')
        </main>

        <script src="{{asset('js/libs/jquery/jquery-1.11.2.min.js')}}"></script>
        <script src="{{asset('js/libs/materialize/js/materialize.min.js')}}"></script>
        <script type="application/javascript" src="{{asset('js/general.js')}}"></script>
        @yield('scripts')
    </body>
</html>