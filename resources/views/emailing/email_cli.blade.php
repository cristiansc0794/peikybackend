<div style="background-color: #F3F3F3; margin-top: 0; ">
	<div style="width: 49%; display: block; height: 100%; background-color: #fff; margin: 0 auto; box-shadow: -1px -2px 10px #D1D1D1; padding--bottom: 5%;">
		<!-- <img style="width: 100%;" src="{{ $message->embed('http://200.119.44.245:4041/codensa/imagenes/cliente-horizontal.jpg') }}" alt="logo">} -->

		<h1 style="margin-top: 40px; text-align: center;font-family: 'Arial';">
		BIENVENIDO/A A PRO
		</h1>

		<p style="margin-top: 20px;color: #B4B4B4; text-align: center; font-family: 'Arial'; font-size: 100%;">
			Nos complace que hayas decidido <br>formar parte de nuestra comunidad
		</p>

		<p style="margin-top: 30px;color: #B4B4B4; text-align: center; font-family: 'Arial'; font-size: 100%;">
			La siguiente es tu contrase&ntilde;a para ingresar<br>a la plataforma pero puedes cambiarla en la secci&oacute;n <br>
			<a href="#" style="color: #FF0F64; text-decoration: none;">Mi Perfil</a>
		</p>


		<div style="margin-top: 10px; width: 100%; text-align: center;">
			<span style="background-color: #F3F3F3; font-family: 'Arial'; font-size: 30px; color: #3A3A3A;padding:5px 25px 5px 25px;">

			</span>
		</div>

		<p style="color: black; text-align: center; font-family: 'Arial'; font-size: 18px;margin-top: 50px;">
			Para completar el registro a la plataforma <br>haz clic en el siguiente bot&oacute;n
		</p>

		<div style="margin: 0 auto; height: auto; width: 250px;">
			<a href="{{url('login')}}" style="margin:0 auto;">
				<button type="button" style="margin:0 auto; background-color: #41B9E6;height: 60px;width: 250px;border:none;color:white;font-family: 'Arial';font-size: 30px;margin-top: 10px;">
					Ingresar
				</button>
			</a>
		</div>
	</div>
</div>
