
<!DOCTYPE html>
<html>
	@include('template.layout.head')

	<body class="menubar-hoverable header-fixed ">
 		@include('template.layout.header')
 		
		@if(Auth::user()->type_users == "admin")
        <div id="base">

			<!-- BEGIN OFFCANVAS LEFT -->
			<div class="offcanvas">
			</div><!--end .offcanvas-->
			<!-- END OFFCANVAS LEFT -->

			<!-- BEGIN CONTENT-->
			<div id="content">
				@yield('contenido_cliente')
			</div><!--end #content-->
			<!-- END CONTENT -->

			<!-- BEGIN MENUBAR-->
			<div id="menubar" class="menubar-inverse ">
				<div class="menubar-fixed-panel">
					<div>
						<a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
							<i class="fa fa-bars"></i>
						</a>
					</div>
					<div class="expanded">
						<a href="../../html/dashboards/dashboard.html">
							<span class="text-lg text-bold text-primary ">PEYKI ADMIN</span>
						</a>
					</div>
				</div>
				<div class="menubar-scroll-panel">

					<!-- BEGIN MAIN MENU -->
					<ul id="main-menu" class="gui-controls">

						<!-- BEGIN DASHBOARD -->
						<li>
							<a href="{{url('admin/indexadmin')}}" class="active">
								<div class="gui-icon"><i class="md md-home"></i></div>
								<span class="title">Dashboard</span>
							</a>
						</li><!--end /menu-li -->
						<!-- END DASHBOARD -->

						<!-- BEGIN Usuaro -->
						<li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="fa fa-user"></i></div>
								<span class="title">Usuarios</span>
							</a>
							<!--start submenu -->
							<ul>
								<li><a href="{{url('admin/user')}}" ><span class="title">Ver</span></a></li>
								<li><a href="{{url('admin/create')}}" ><span class="title">Crear</span></a></li>
							</ul><!--end /submenu -->
						</li><!--end /menu-li -->
						<!-- END Usuario -->

						<!-- BEGIN Tienda -->
						<li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="fa fa-cart-arrow-down"></i></div>
								<span class="title">Tiendas</span>
							</a>
							<!--start submenu -->
							<ul>
								<li><a href="{{url('admin/store')}}" ><span class="title">Ver</span></a></li>
								<li><a href="{{url('admin/store/create')}}" ><span class="title">Crear</span></a></li>
							</ul><!--end /submenu -->
						</li><!--end /menu-li -->
						
						<!--<li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="fa fa-lemon-o"></i></div>
								<span class="title">Productos</span>
							</a>

							<ul>
								<li><a href="{{url('admin/product')}}" ><span class="title">Ver</span></a></li>
								<li><a href="{{url('admin/product/create')}}" ><span class="title">Crear</span></a></li>
							</ul>
						</li>--><!--end /menu-li -->
						<!-- END Usuario -->

					</ul><!--end .main-menu -->
					<!-- END MAIN MENU -->

					<div class="menubar-foot-panel">
						<small class="no-linebreak hidden-folded">
							<span class="opacity-75">Copyright &copy; 2014</span> <strong>CodeCovers</strong>
						</small>
					</div>
				</div><!--end .menubar-scroll-panel-->
			</div><!--end #menubar-->
			<!-- END MENUBAR -->

			
			<!-- END OFFCANVAS RIGHT -->

		</div><!--end #base-->

		@else()
			<center>
				<div class="alert alert-info" role="alert"><span class="glyphicon glyphicon-ban-circle"></span> No tiene suficiente permiso para ingresar a esta sección <br><br><a href="{{url('proveedor/clientes')}}" > volver</a> </div>
			</center>
		@endif()

		@include('template.layout.scripts')
	</body>
</html>