<header id="header">
    <div class="headerbar">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="headerbar-left">
            <ul class="header-nav header-nav-options">
                <li class="header-nav-brand" >
                    <div class="brand-holder">
                        <a href="{{ url('admin/index') }}">
                            <span class="text-lg text-bold text-primary">PEIKY ADMIN</span>
                        </a>
                    </div>
                </li>
                <li>
                    <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
            </ul>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                @else
                    <div class="headerbar-right">
                        <ul class="header-nav header-nav-profile">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
                                    <img src="https://cdn2.iconfinder.com/data/icons/website-icons/512/User_Avatar-512.png" alt="">
                                    <span class="profile-info">
                            {{ Auth::user()->name }}
                                        <small>{{ Auth::user()->type_users }} </small>
                        </span>
                                </a>
                                <ul class="dropdown-menu animation-dock">
                                    <li><a href="{{ url('admin/logout') }}"
                                           onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a>
                                        <form id="logout-form" action="{{ url('admin/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                    <li>
                                    <a href="{{ url('admin/edit/'.Auth::user()->id) }}"><i class="fa fa-fw fa-cog"></i> Editar Perfil</a>
                                    </li>
                                </ul><!--end .dropdown-menu -->
                            </li><!--end .dropdown -->
                        </ul><!--end .header-nav-profile -->
                    </div>
                @endif
            </ul>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        
    </div>
</header>