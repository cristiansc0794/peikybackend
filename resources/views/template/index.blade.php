<!DOCTYPE html>
<html>
	@include('template.layout.head')
	<body>
		<div class="container">

			{{-- variable para el contenido --}}
			
			@yield('contenido')
		</div>
		@yield('scripts')
	</body>
</html>